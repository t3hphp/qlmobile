// JavaScript Document (của thầy)
$(document).ready(function() {
    // Hiển thị hình khi upload
    $("#hinh").on('change', function () {

    	if (typeof (FileReader) != "undefined") {

    		var image_holder = $("#image-holder");
    		image_holder.empty();

    		var reader = new FileReader();
    		reader.onload = function (e) {
    			$("<img />", {
    				"src": e.target.result,
    				"class": "thumb-image",
    				"width": "200px",
    			}).appendTo(image_holder);

    		}
    		image_holder.show();
    		reader.readAsDataURL($(this)[0].files[0]);
    	} else {
    		alert("This browser does not support FileReader.");
    	}
	});// end Hiển thị hình khi upload

	// Datepicker
	$(function() {
		$('#datetimepicker1').datetimepicker({
			format: 'dd-mm-yy',
		});
	});
	/*$(".datepicker").datepicker();
	$(".datepicker").datepicker( "option", "dateFormat", "dd-mm-yy" )*/;
}); // end ready

// Hàm kiểm tra
function Kiemtradulieu()
{
	var kt=document.getElementsByClassName("kiemtra");
	for(i=0;i<kt.length;i++)
	{
		if(kt.item(i).value=="")
		{
			alert(kt.item(i).getAttribute("data_error"));
			kt.item(i).focus();
			return false; 	
		}	
	}
	return true;
}

//== jQuery for fun
//Hiển thị bảng thông báo khi xóa item
$(document).ready(function() {
	$("#alert-delete-item-success").hide();
	$("#alert-delete-item-fail").hide();
});

function show_alert_success()
{
	$("#alert-delete-item-success").fadeTo(2000, 500).slideUp(500, function(){
		$("#alert-delete-item-success").slideUp(500);
	});
}

function show_alert_fail()
{
	$("#alert-delete-item-fail").fadeTo(2000, 500).slideUp(500, function(){
		$("#alert-delete-item-fail").slideUp(500);
	});
}

//curency-->
$(function()
{
	$('.currency').maskMoney({
		precision: 0,
	});
});

