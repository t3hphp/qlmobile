/*Ajax in here*/
//Cách viết khác: $(function() {} )
$(document).ready(function()
{
	//========XÓA ĐIỆN THOẠI
	//dùng attr class
	$('.xoa-dien-thoai').click(function()
	{
		var id= $(this).attr('ma_dien_thoai'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-dienthoai.php",
				data: data,
				cache: false,
				success: function(){
					show_alert_success(); parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){ show_alert_fail(); }
			});//end ajax
		}//end ifif
	});

	//========XÓA NHÀ SẢN XUẤT
	//dùng attr class
	$('.xoa-nha-sx').click(function()
	{
		var id= $(this).attr('ma_nha_san_xuat'); //attr này tự đặt (tên tùy ý)
		var data = 'id=' + id ;
        var parent = $(this).parent().parent(); //parent1: <td> -> parent2: <tr>
		//alert(data);
		if(confirm("Dữ liệu xóa sẽ không khôi phục được, bạn chắc chắn xóa?"))
		{
			$.ajax(
			{
				type: "POST", //chỗ này dấu PHẨY (,)
				url: "xoa-nhasx.php",
				data: data,
				cache: false,
				success: function(){
					show_alert_success(); parent.fadeOut('slow', function() {$(this).remove();});
				},
				error: function(){ show_alert_fail(); }
			});//end ajax
		}//end ifif
	});

	//========Xử lý hóa đơn
	$('.a-xem-hoa-don').click(function()
	{
		var id= $(this).attr('ma_khach_hang');
		var data = 'id=' + id ;
		$.ajax(
		{
			type: "POST", //chỗ này dấu PHẨY (,)
			url: "xl_hoa_don.php",
			data: data,
			cache: false,
			success: function(dataBack)
			{
				$('.xem-ct-hoa-don').hide();
				$('.xem-hoa-don').html(dataBack);
			}
		});//end ajax
	});

	//========Xử lý chi tiết hóa đơn
	$('.xem-ct-hoa-don-ne').click(function()
	{
		var id= $(this).attr('ma_hoa_don');
		var data = 'id=' + id ;
		$.ajax(
		{
			type: "POST", //chỗ này dấu PHẨY (,)
			url: "xl_ct_hoa_don.php",
			data: data,
			cache: false,
			success: function(dataBack)
			{
				$('.xem-ct-hoa-don').show();
				$('.xem-ct-hoa-don').html(dataBack);
			}
		});//end ajax
	});

	//========Xử lý chi tiết hóa đơn
	$('.a-xem-thong-tin').click(function()
	{
		var id= $(this).attr('ma_khach_hang');
		var data = 'id=' + id ;
		$.ajax(
		{
			type: "POST", //chỗ này dấu PHẨY (,)
			url: "xl_khach_hang.php",
			data: data,
			cache: false,
			success: function(dataBack)
			{
				$('.modal-dialog').html(dataBack);
			}
		});//end ajax
	});
	
}) //end document
