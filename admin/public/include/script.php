<!--script-->
<!-- Bootstrap Core JavaScript -->
<script src="public/layout/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="public/layout/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="public/layout/vendor/raphael/raphael.min.js"></script>
<script src="public/layout/vendor/morrisjs/morris.min.js"></script>
<script src="public/layout/data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="public/layout/dist/js/sb-admin-2.js"></script>

<script src="public/assets/datepicker/js/bootstrap-datepicker.min.js"></script>

<script src="public/assets/jquery.maskMoney.min.js"></script>

<!--JS tự mò-->
<script src="public/assets/jsplus.js"></script>
<script src="public/assets/thu_vien_ajax.js"></script>

<script src="public/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    //TinyMCE
    tinymce.init({
        selector: '.tinymce',
        plugins : 'advlist autolink link image lists charmap print preview',
        height : '480',
    });
</script>