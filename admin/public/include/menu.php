<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
         <li>
            <a href="."><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>

        <li>
            <a href="#"><i class="fa fa-phone-square fa-fw"></i> Điện Thoại<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="dienthoai.php">Danh sách điện thoại</a>
                </li>
                <li>
                    <a href="them-dienthoai.php">Thêm mới điện thoại <i class="fa fa-plus-circle fa-fw" aria-hidden="true"></i></a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>

        <li>
            <a href="#"><i class="fa fa-university fa-fw"></i> Nhà Sản Xuất<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="nhasx.php">Danh sách NSX</a>
                </li>
                <li>
                    <a href="them-nhasx.php">Thêm mới NSX <i class="fa fa-plus-circle fa-fw" aria-hidden="true"></i></a>
                </li>
            </ul>
        </li>

        <li>
            <a href="qlkhhd.php"><i class="fa fa-user fa-fw"></i> Quản lý KH & Hóa đơn</a>
        </li>

    </ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->