<!DOCTYPE html>
<html lang="en">

<?php include("head.php") ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        
        <?php include("nav-head.php") ?>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        
        <?php include("menu.php") ?>
        <!-- /.navbar-static-side --> <!--a.k.a Menu Right-->
        
        </nav>
        
        <?php include ("content.php") ?>
        <!-- /#page-wrapper --> <!--a.k.a Content-->
        
        </div>
    <!-- /#wrapper -->

	<!--script-->
    <?php include("script.php") ?>
	
</body>

</html>