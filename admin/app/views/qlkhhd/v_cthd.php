<div class="table-responsive table-bordered bg-success">
	<table class="table">
		<thead>
			<tr>
				<th>Điện thoại</th>
				<th>Số lượng</th>
				<th>Đơn giá</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($cthds as $cthd) { ?>
			<tr class="dropdown">
				<td><?php echo $cthd->ten_dien_thoai ?></td>
				<td><?php echo $cthd->so_luong ?></td>
				<td><?php echo number_format($cthd->don_gia) ?> VNĐ</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<br/><br/>
