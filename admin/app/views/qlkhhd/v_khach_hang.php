<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Thông tin khách hàng <strong><?php echo $kh->ho_ten ?></strong></h4>
  </div>
  <div class="modal-body" style="font-size:18px; color:#a2a2a2">
    <span class="label label-success" style="margin:20px 10px">Mã KH</span>
    <?php echo $kh->ma_khach_hang ?>
    <br/>

    <span class="label label-primary" style="margin:20px 10px">Họ tên</span>
    <?php echo $kh->ho_ten ?>
    <br/>

    <span class="label label-default" style="margin:20px 10px">Tài khoản</span>
    <?php echo $kh->tai_khoan ?>
    <br/>

    <span class="label label-warning" style="margin:20px 10px">Email</span>
    <?php echo $kh->email ?>
    <br/>

    <span class="label label-danger" style="margin:20px 10px">Địa chỉ</span>
    <?php echo $kh->dia_chi ?>
    <br/>

    <span class="label label-info" style="margin:20px 10px">Điện thoại</span>
    <?php echo $kh->dien_thoai ?>
    <br/>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
  </div>
</div>