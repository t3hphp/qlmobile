<div class="row">
	<div class="col-md-8">
		<div class="form-group form-gr-tim">
			<form method="post">
				<input type="text" class="form-control form-ctrl-tim" name="tim_kiem" placeholder="Nhập tên khách hàng...">
				<input type="submit" name="btn_tim_kiem" class="btn btn-outline btn-success" value="Tìm"/>
			</form>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="table-responsive table-bordered">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Họ tên</th>
						<th>Tài khoản</th>
						<th>Điện thoại</th>
						<th>&nbsp;</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($khs as $kh) { ?>
					<tr class="dropdown">
						<td><?php echo $kh->ma_khach_hang ?></td>
						<td><?php echo $kh->ho_ten ?></td>
						<td><?php echo $kh->tai_khoan ?></td>
						<td><?php echo $kh->dien_thoai ?></td>
						<td>
							<button ma_khach_hang="<?php echo $kh->ma_khach_hang ?>" class="btn btn-default a-xem-thong-tin" data-toggle="modal" data-target="#xemKH">Xem thông tin</button>
							<a ma_khach_hang="<?php echo $kh->ma_khach_hang ?>" class="btn btn-info a-xem-hoa-don">Xem hóa đơn</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-md-5">
		<div class="xem-hoa-don"></div>
		<hr/>
		<div class="xem-ct-hoa-don"></div>
	</div>
</div>

<!-- Modal -->
<div id="xemKH" class="modal fade" role="dialog">
  <div class="modal-dialog">

  </div>
</div>