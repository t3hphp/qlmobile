<div class="table-responsive table-bordered bg-info">
	<table class="table">
		<h4 style="padding:10px">Danh sách hóa đơn của khách hàng <strong class="text-danger"><?php echo $ten_khach_hang->ho_ten ?></strong></h4>
		<?php 
		if(count($hds) == 0){
			echo '<p style="margin-left:20px">Khách hàng này chưa có mua hàng</p>';
		}
		else
		{
			?>
			<thead>
				<tr>
					<th>Mã HĐ</th>
					<th>Ngày đặt hàng</th>
					<th>Tổng tiền</th>
					<th>Tình trạng</th>
					<th>&nbsp;</th>
				</tr>
			</thead>

			<tbody>

				<?php foreach($hds as $hd) { ?>
				<tr class="dropdown">
					<td><?php echo $hd->ma_hoa_don ?></td>
					<td><?php echo $hd->ngay_dat_hang ?></td>
					<td width="120px"><?php echo number_format($hd->tong_tien) ?> VNĐ</td>
					<td>
						<?php if($hd->trang_thai == 0)
						{
							echo '<p class="label label-default">Mới đặt hàng</p>';
						}
						elseif ($hd->trang_thai == 1)
						{
							echo '<p class="label label-warning">Đã xác nhận</p>';
						}
						else
						{
							echo '<p class="label label-success">Hoàn tất !!!</p>';
						}
						?>
						<!--nút cập nhật tình trạng hóa đơn-->
						<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalCapNhatTT<?php echo $hd->ma_hoa_don ?>" style="padding:0 5px"><i class="fa fa-cog" aria-hidden="true" style="font-size:10px"></i></button>

						<!-- Modal -->
						<div id="modalCapNhatTT<?php echo $hd->ma_hoa_don ?>" class="modal fade" role="dialog">
							<div class="modal-dialog modal-sm">

								<!-- Modal content-->
								<div class="modal-content">
									<form method="post">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Cập nhật tình trạng hóa đơn <strong><?php echo $hd->ma_hoa_don ?></strong></h4>
										</div>
										<div class="modal-body">
											<!--vì id chỉ có 1 element được kích hoạt ->chèn thêm id hóa đơn phía sau-->
											<input type="hidden" name="ma_hoa_don_ne" value="<?php echo $hd->ma_hoa_don ?>" />
											<?php $tt = $hd->trang_thai; ?>
											<input type="radio" name="tinhtrang" id="moi<?php echo $hd->ma_hoa_don ?>" value="0" <?php echo ($tt == 0) ? "checked" : "" ?>/>
											<label for="moi<?php echo $hd->ma_hoa_don ?>" class="text-info">Mới đặt hàng</label>
											<br/>

											<input type="radio" name="tinhtrang" id="nhan<?php echo $hd->ma_hoa_don ?>" value="1" <?php echo ($tt == 1) ? "checked" : "" ?>/>
											<label for="nhan<?php echo $hd->ma_hoa_don ?>" class="text-warning">Đã xác nhận</label>
											<br/>

											<input type="radio" name="tinhtrang" id="ok<?php echo $hd->ma_hoa_don ?>" value="2" <?php echo ($tt == 2) ? "checked" : "" ?>/>
											<label for="ok<?php echo $hd->ma_hoa_don ?>" class="text-success">Hoàn tất !!!</label>
										</div>
										<div class="modal-footer">
											<input type="submit" class="btn btn-warning" name="btnCapNhatTT" value="Cập nhật">
										</div>
									</form>
								</div>

							</div>
						</div>
					</td>
					<td>
						<a ma_hoa_don="<?php echo $hd->ma_hoa_don ?>" class="xem-ct-hoa-don-ne btn btn-sm btn-default">Chi tiết</a>
					</td>
				</tr>
				<?php }
			} ?>
		</tbody>
	</table>
</div>

<!-- jQuery -->
<script src="public/layout/vendor/jquery/jquery.min.js"></script>
<script src="public/assets/thu_vien_ajax.js"></script>