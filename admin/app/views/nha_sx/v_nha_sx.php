<div class="row">
	<div class="col-md-8">
		<div class="form-group form-gr-tim">
			<form method="post">
				<input type="text" class="form-control form-ctrl-tim" name="tim_kiem" placeholder="Nhập tên nsx...">
				<input type="submit" name="btn_tim_kiem" class="btn btn-outline btn-success" value="Tìm"/>
			</form>
		</div>
	</div>
</div>
<div class="row">
	<div class="table-responsive table-bordered">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Tên nhà sản xuất</th>
					<th>Logo</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($nha_sxes as $nsx)
				{ ?>
				<tr>
					<td><?php echo $nsx->ma_nha_san_xuat ?></td>
					<td><strong><?php echo $nsx->ten_nha_san_xuat ?></strong></td>
					<td><img src="../images/brand/<?php echo $nsx->logo ?>" width="120px"></td>
					<td>
						<a href="sua-nhasx.php?nsx=<?php echo $nsx->ma_nha_san_xuat ?>" class="btn btn-warning btn-circle"><i class="fa fa-gear"></i>
						</a><br/><br/>
						<button ma_nha_san_xuat="<?php echo $nsx->ma_nha_san_xuat ?>" class="xoa-nha-sx btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="alert alert-success alert-xoa" id="alert-delete-item-success">
			Xóa thành công!
		</div>

		<div class="alert alert-danger alert-xoa" id="alert-delete-item-fail">
			<strong>Xóa không thành công!</strong>
		</div>
	</div>
</div>