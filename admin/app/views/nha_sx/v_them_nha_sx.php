<div class="row">
	<form role="form" method="post" enctype="multipart/form-data">
		<div class="form-group"> 
			<label>Tên nhà sản xuất</label>
			<input class="form-control kiemtra" name="ten_nha_san_xuat" data_error="Nhập tên nhà sản xuất">
		</div>

		<div class="form-group">
			<label>Logo nhà sản xuất</label>
			<input type="file" class="kiemtra" name="logo" id="logo" data_error="Chọn logo">
			<div class="image-holder" id="image-holder"></div>
		</div>

		<div class="pull-right">
			<input type="submit" value="Thêm mới" name="btn_them_moi" class="btn btn-primary btn-lg" onclick="return Kiemtradulieu()"/>
			<input type="button" value="Bỏ qua" class="btn btn-outline btn-primary btn-lg" onclick="window.location='nhasx.php'"/>
		</div>
	</form>
</div>