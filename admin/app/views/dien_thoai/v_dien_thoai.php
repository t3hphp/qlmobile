<div class="row">
	<div class="col-md-8">
		<div class="form-group form-gr-tim">
			<form method="post">
				<input type="text" class="form-control form-ctrl-tim" name="tim_kiem" placeholder="Nhập tên điện thoại...">
				<input type="submit" name="btn_tim_kiem" class="btn btn-outline btn-success" value="Tìm"/>
			</form>
		</div>
	</div>

	<div class="col-md-4">
		<div class="text-right" style="margin-top:-20px">
		<ul class="pagination"><?php echo isset($list)?$list:"" ?></ul>
		</div>
	</div>
</div>

<div class="row">
	<div class="table-responsive table-bordered">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Hình</th>
					<th>Tên ĐT</th>
					<th>Mô tả</th>
					<th style="text-align: center">Đơn giá <span class='bg-danger'>(KM)</span></th>
					<th style="text-align: center">Số lượt mua</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($dts as $dt)
				{ ?>
				<tr>
					<td><?php echo $dt->ma_dien_thoai ?></td>
					<td width="180px"><img src="../images/<?php echo $dt->hinh ?>" width="120px"></td>
					<td width="250px"><strong><?php echo $dt->ten_dien_thoai ?></strong> <br/> <span class="label label-default"> <?php echo $dt->ten_nha_san_xuat ?></span></p></td>
					<td width="380px" style="font-size:11px; text-align: justify;"><?php echo htmlspecialchars(substr($dt->mo_ta,0,300)) . "..." ?></td>
					<td style="font-size:18px; text-align: center">
						<?php
						if($dt->don_gia_khuyen_mai>0)
						{
							echo number_format($dt->don_gia) . "<br/> <p class='bg-danger' style='display: inline;padding: 5px;'><strong>" . number_format($dt->don_gia_khuyen_mai) . "</strong></p>";
						}
						else
						{
							echo number_format($dt->don_gia);
						}
						?>
					</td>
					<td style="font-size:22px; font-weight: bold; text-align: center"><?php echo $dt->so_luot_mua ?></td>
					<td>
						<a href="sua-dienthoai.php?ma_dien_thoai=<?php echo $dt->ma_dien_thoai ?>" class="btn btn-warning btn-circle"><i class="fa fa-gear"></i>
						</a><br/><br/>
						<button ma_dien_thoai="<?php echo $dt->ma_dien_thoai ?>" class="xoa-dien-thoai btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="alert alert-success alert-xoa" id="alert-delete-item-success">
			Xóa thành công <strong><?php echo $dt->ten_dien_thoai ?></strong>!
		</div>

		<div class="alert alert-danger alert-xoa" id="alert-delete-item-fail">
			<strong>Xóa không thành công!</strong>
		</div>
	</div>
</div>

<div class="row">
	<div class="text-center">
		<ul class="pagination"><?php echo isset($list)?$list:"" ?></ul>
	</div>
</div>