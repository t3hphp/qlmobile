<?php 
include("app/models/m_nha_sx.php");
$m_nha_sx = new M_nha_sx();
$nsxs = $m_nha_sx->Doc_nha_sx();
?>
<div clas="row">
	<!--`ma_dien_thoai`, `ma_nha_san_xuat`, `ten_dien_thoai`, `mo_ta`, `tom_tat_thong_so`, `hinh`, `don_gia`, `don_gia_khuyen_mai`, `so_luot_xem`, `so_luot_mua`, `ngay_cap_nhat`, `bao_hanh`, `con_hang`-->
	<form role="form" method="post" enctype="multipart/form-data">
		<div class="col-md-8">
			<div class="form-group"> 
				<label>Tên điện thoại</label>
				<input class="form-control kiemtra" name="ten_dien_thoai" data_error="Nhập tên điện thoại">
			</div>

			<div class="form-group"> 
				<label>Tên nhà sản xuất</label>
				<select class="form-control" name="ma_nha_san_xuat">
					<?php foreach($nsxs as $nsx) { ?>
					<option value="<?php echo $nsx->ma_nha_san_xuat ?>"><?php echo $nsx->ten_nha_san_xuat ?></option>
					<?php } ?> 
				</select>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Đơn giá</label>
					<div class="form-group input-group">
						<input type="text" class="form-control currency kiemtra" name="don_gia" data_error="Nhập đơn giá">
						<span class="input-group-addon">VNĐ</span>
					</div>
				</div>

				<div class="col-md-6">
					<label><strong>Đơn giá khuyến mãi</strong></label>
					<div class="form-group input-group">
						<input type="text" class="form-control currency" name="don_gia_khuyen_mai">
						<span class="input-group-addon">VNĐ</span>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label>Mô tả - Giới thiệu</label>
				<textarea class="form-control tinymce" rows="5" name="mo_ta"></textarea>
			</div>

			<div class="form-group">
				<label>Tóm tắt thông số</label>
				<textarea class="form-control tinymce" rows="3" name="tom_tat_thong_so"></textarea>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Hình điện thoại</label>
						<input type="file" name="hinh" id="hinh" class="kiemtra" data_error="Chọn hình điện thoại">
					</div>
				</div>

				<div class="col-md-6">
					<div class="image-holder" id="image-holder"></div>
				</div>
			</div>
			<div class="form-group"> 
				<label>Bảo hành</label>
				<div class="form-group input-group">
					<input type="number" class="form-control kiemtra" value="12" name="bao_hanh" data_error="Nhập thời gian bảo hành">
					<span class="input-group-addon">tháng</span>
				</div>
			</div>

			<div class="form-group"> 
				<label>Ngày cập nhật</label>
				<div class="form-group">
					<div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
						<input type="text" class="form-control kiemtra" name="ngay_cap_nhat" data_error="Nhập ngày cập nhật">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="disabledSelect">Số lượt mua</label>
						<input class="form-control" id="disabledInput" type="text" placeholder="0" readonly>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="disabledSelect">Số lượt xem</label>
						<input class="form-control" id="disabledInput" type="text" placeholder="1" readonly>
					</div>
				</div>
			</div>

			<br/>
			<!--Button-->
			<div class="pull-right">
				<input type="submit" value="Thêm mới" name="btn_them_moi" class="btn btn-primary btn-lg" onclick="return Kiemtradulieu()"/>
				<input type="button" value="Bỏ qua" class="btn btn-outline btn-primary btn-lg" onclick="window.location='dienthoai.php'"/>
			</div>
		</div>
	</form>
</div>