<?php 
@session_start();
class C_nha_sx
{
	public function Hien_thi_nha_sx()
	{
		//Model
		include("app/models/m_nha_sx.php");
		$m_nha_sx = new M_nha_sx();
		$nha_sxes = $m_nha_sx->Doc_nha_sx();
		//Không phân trang vì số lượng ít (<25)

		//Tìm nhà sản xuất
		if(isset($_POST["btn_tim_kiem"]))
		{
			$tim = $_POST["tim_kiem"];
			$nha_sxes = $m_nha_sx->Tim_nha_sx($tim);
		}

		//View
		$title = "Danh sách nhà sản xuất :: QLMobile";
		$tieu_de_trang = "Danh sách nhà sản xuất";
		$view = "app/views/nha_sx/v_nha_sx.php";
		include("public/include/layout.php");
	}

	//`ma_nha_san_xuat`, `ten_nha_san_xuat`, `logo`
	public function Them_nha_sx()
	{
		//Model
		include("app/models/m_nha_sx.php");
		$m_nha_sx = new M_nha_sx();
		if(isset($_POST["btn_them_moi"]))
		{
			$ten_nha_san_xuat = $_POST["ten_nha_san_xuat"];
			$logo = $_FILES["logo"]["error"]==0?$_FILES["logo"]["name"]:"";
			$kq = $m_nha_sx->Them_nha_sx($ten_nha_san_xuat, $logo);
			if($kq)
			{
				//Upload hình
				if($_FILES["logo"]["error"]==0) //Có chọn hình
				{
					//hàm chuyển file về thư mục hình
					$kq=move_uploaded_file($_FILES["logo"]["tmp_name"], "../images/brand/$logo");
				}
				echo "<script>alert('Thêm thành công'); window.location='nhasx.php';</script>";
			}
			else
			{
				echo "<script>alert('Thêm không thành công');</script>";
			}
		}
	
		//View
		$title = "Thêm mới nhà sản xuất :: QLMobile";
		$tieu_de_trang = "Thêm mới nhà sản xuất";
		$view = "app/views/nha_sx/v_them_nha_sx.php";
		include("public/include/layout.php");
	}

	public function Sua_nha_sx()
	{
		//Model
		include("app/models/m_nha_sx.php");
		$m_nha_sx = new M_nha_sx();
		$ma_nsx = $_GET["nsx"];
		$nsx = $m_nha_sx->Doc_nha_sx_theo_ma($ma_nsx);
		if(isset($_POST["btn_cap_nhat"]))
		{
			$ten_nha_san_xuat = $_POST["ten_nha_san_xuat"];
			$logo = $_FILES["logo"]["error"]==0?$_FILES["logo"]["name"]:$nsx->logo; //nếu không thay đổi thì giữ lại hình cũ
			$kq = $m_nha_sx->Sua_nha_sx($ten_nha_san_xuat, $logo, $ma_nsx);
			if($kq)
			{
				//Upload hình
				if($_FILES["logo"]["error"]==0) //Có chọn hình
				{
					//hàm chuyển file về thư mục hình
					$kq=move_uploaded_file($_FILES["logo"]["tmp_name"], "../images/brand/$logo");
				}
				echo "<script>alert('Sửa thành công'); window.location='nhasx.php';</script>";
			}
			else
			{
				echo "<script>alert('Sửa không thành công');</script>";
			}
		}

		//View
		$title = "Sửa nhà sản xuất :: QLMobile";
		$tieu_de_trang = "Sửa nhà sản xuất";
		$view = "app/views/nha_sx/v_sua_nha_sx.php";
		include("public/include/layout.php");
	}
}

?>