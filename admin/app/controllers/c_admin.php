<?php 
@session_start();
require_once("app/models/m_admin.php");
class C_admin
{
	public function Hien_thi_dang_nhap()
	{
		//isset button login
		if(isset($_POST["login"]))
		{
			$tai_khoan = $_POST["tai_khoan"];
			$mat_khau = md5($_POST["mat_khau"]);
			$this->Kiem_tra_admin($tai_khoan, $mat_khau);
		}

		//true
		if(isset($_SESSION["admin"]))
		{
			$title = "CMS QLMobile";
			include("public/include/layout.php");
		}
		else
		{
			header("location:login.php");
		}
	}

	function Kiem_tra_admin($tai_khoan, $mat_khau)
	{
		$m_admin = new M_admin();
		$admin = $m_admin->Lay_tai_khoan_admin();
		if($tai_khoan != $admin->tai_khoan)
		{
			$_SESSION["error"]="Tài khoản không đúng!";
			header("location:login.php");
		}
		elseif ($mat_khau != $admin->mat_khau)
		{
			$_SESSION["error"]="Mật khẩu không đúng!";
			header("location:login.php");
		}
		else //all passed
		{
			$_SESSION["admin"] = $admin->ho_ten;
		}
	}

	public function Thoat_dang_nhap()
	{
		session_destroy();
		header("location:login.php");
	}
}
 ?>