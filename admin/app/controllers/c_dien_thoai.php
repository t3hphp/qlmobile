<?php 
@session_start();
include_once("app/models/m_dien_thoai.php");
class C_dien_thoai
{
	public function Hien_thi_dien_thoai()
	{
		//Model
		$m_dien_thoai = new M_dien_thoai();
		$dts = $m_dien_thoai->Doc_dien_thoai();

		//Tìm điện thoại (ko phân trang)
		if(isset($_POST["btn_tim_kiem"]))
		{
			$tim = $_POST["tim_kiem"];
			$dts = $m_dien_thoai->Tim_dien_thoai($tim);
		}
		else
		{
			//Phân trang
			include("../config/Pager.php");
			$p = new pager();
			$limit = 8;
			$count = count($dts);
			$vt = $p->findStart($limit);
			$pages = $p->findPages($count, $limit);
			$curpage = $_GET["page"];
			$list = $p->pageList($curpage, $pages);
			$dts = $m_dien_thoai->Doc_dien_thoai($vt, $limit);
		}

		//View
		$title = "Danh sách điện thoại :: QLMobile";
		$tieu_de_trang = "Danh sách điện thoại";
		$view = "app/views/dien_thoai/v_dien_thoai.php";
		include("public/include/layout.php");
	}

	//ma_dien_thoai, ma_nha_san_xuat, ten_dien_thoai, mo_ta, tom_tat_thong_so, hinh, don_gia, don_gia_khuyen_mai, so_luot_xem, so_luot_mua, ngay_cap_nhat, bao_hanh, con_hang

	public function Them_dien_thoai()
	{
		//Model
		$m_dien_thoai = new M_dien_thoai();
		if(isset($_POST["btn_them_moi"]))
		{
			$ma_nha_san_xuat = $_POST["ma_nha_san_xuat"]; //option
			$ten_dien_thoai = $_POST["ten_dien_thoai"];
			$mo_ta = $_POST["mo_ta"];
			$tom_tat_thong_so = $_POST["tom_tat_thong_so"];
			$hinh = $_FILES["hinh"]["error"]==0?$_FILES["hinh"]["name"]:"";
			$don_gia = str_replace(',', '',$_POST["don_gia"]);
			$don_gia_khuyen_mai = ($_POST["don_gia_khuyen_mai"]=="")?(NULL):str_replace(',', '',($_POST["don_gia_khuyen_mai"]));
			//$ngay_cap_nhat = date("Y-m-d", strtotime($_POST["ngay_cap_nhat"]));
			$ngay_cap_nhat = $_POST["ngay_cap_nhat"];
			//echo $ngay_cap_nhat;
			$bao_hanh = $_POST["bao_hanh"];

			$kq = $m_dien_thoai->Them_dien_thoai($ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, 1, 0, $ngay_cap_nhat, $bao_hanh);
			//INSERT INTO `dien_thoai`(`ma_dien_thoai`, `ma_nha_san_xuat`, `ten_dien_thoai`, `mo_ta`, `tom_tat_thong_so`, `hinh`, `don_gia`, `don_gia_khuyen_mai`, `so_luot_xem`, `so_luot_mua`, `ngay_cap_nhat`, `bao_hanh`, `con_hang`) VALUES (NULL,3,"OPPO GR5","a","b","abc.jpg",21,22,1,0,"2010-02-06",12,1)
			//print_r($kq);
			if($kq)
			{
				//Upload hình
				if($_FILES["hinh"]["error"]==0) //Có chọn hình
				{
					//hàm chuyển file về thư mục hình
					$kq=move_uploaded_file($_FILES["hinh"]["tmp_name"], "../images/$hinh");
				}
				echo "<script>alert('Thêm thành công'); window.location='dienthoai.php';</script>";
			}
			else
			{
				echo "<script>alert('Thêm không thành công');</script>";
			}
		}

		//View
		$title = "Thêm mới điện thoại :: QLMobile";
		$tieu_de_trang = "Thêm mới điện thoại";
		$view = "app/views/dien_thoai/v_them_dien_thoai.php";
		include("public/include/layout.php");
	}

	public function Sua_dien_thoai()
	{
		//Model
		$ma_dien_thoai = $_GET["ma_dien_thoai"]; //GET mã từ bên v_dien_thoai.php
		$m_dien_thoai = new M_dien_thoai();
		$dt = $m_dien_thoai->Doc_dien_thoai_theo_ma($ma_dien_thoai);
		$hinh_cu = $dt->hinh;
		if(isset($_POST["btn_cap_nhat"]))
		{
			$ma_nha_san_xuat = $_POST["ma_nha_san_xuat"]; //option
			$ten_dien_thoai = $_POST["ten_dien_thoai"];
			$mo_ta = $_POST["mo_ta"];
			$tom_tat_thong_so = $_POST["tom_tat_thong_so"];
			$hinh = $_FILES["hinh"]["error"]==0?$_FILES["hinh"]["name"]:$dt->hinh; //nếu không thay đổi thì giữ lại hình cũ
			$don_gia = $_POST["don_gia"];
			$don_gia_khuyen_mai = ($_POST["don_gia_khuyen_mai"]=="")?(NULL):($_POST["don_gia_khuyen_mai"]);
			$so_luot_xem = $_POST["so_luot_xem"];
			$so_luot_mua = $_POST["so_luot_mua"]; //POST về nhưng ko cho ng` dùng cập nhật
			$ngay_cap_nhat = $_POST["ngay_cap_nhat"];
			$bao_hanh = $_POST["bao_hanh"];
			$kq = $m_dien_thoai->Sua_dien_thoai($ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, $so_luot_xem, $so_luot_mua, $ngay_cap_nhat, $bao_hanh, $ma_dien_thoai);
			if($kq)
			{
				//Upload hình
				if($_FILES["hinh"]["error"]==0) //Có chọn hình
				{
					//hàm chuyển file về thư mục hình
					$kq=move_uploaded_file($_FILES["hinh"]["tmp_name"], "../images/$hinh");
					unlink("../images/$hinh_cu"); //xóa hình cũ
				}
				echo "<script>alert('Cập nhật thành công'); window.location='dienthoai.php';</script>";
			}
			else
			{
				echo "<script>alert('Cập nhật không thành công');</script>";
			}
		}

		//View
		$title = "Sửa điện thoại :: QLMobile";
		$tieu_de_trang = "Sửa điện thoại";
		$view = "app/views/dien_thoai/v_sua_dien_thoai.php";
		include("public/include/layout.php");
	}

	public function Xoa_dien_thoai()
	{
		//Model
		$ma_dien_thoai = $_GET["ma_dien_thoai"]; //GET mã từ bên v_dien_thoai.php
		$m_dien_thoai = new M_dien_thoai();
		$kq = $m_dien_thoai->Xoa_dien_thoai($ma_dien_thoai);
		if($kq)
		{
			echo "<script>alert('Xóa thành công'); window.location='dien_thoai.php'</script>";
		}
		else
		{
			echo "<script>alert('Xóa không thành công'); window.location='dien_thoai.php'</script>";
		}
	}
}
?>