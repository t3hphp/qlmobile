<?php 
@session_start();
include_once("app/models/m_qlkhhd.php");
class C_qlkhhd
{
	public function Hien_thi_qlkhhd()
	{
		//Model
		$m_qlkhhd = new M_qlkhhd();
		$khs = $m_qlkhhd->Doc_khach_hang();

		//Tìm khách hàng
		if(isset($_POST["btn_tim_kiem"]))
		{
			$tim = $_POST["tim_kiem"];
			$khs = $m_qlkhhd->Tim_khach_hang($tim);
		}

		if(isset($_POST["btnCapNhatTT"]))
		{
			$tinhtrang = $_POST["tinhtrang"];
			$ma_hoa_don = $_POST["ma_hoa_don_ne"];
			$kq = $m_qlkhhd->Cap_nhat_tinh_trang_hoa_don($ma_hoa_don, $tinhtrang);
		}

		//View
		$title = "Quản lý khách hàng & hóa đơn :: QLMobile";
		$tieu_de_trang = "Quản lý khách hàng & hóa đơn";
		$view = "app/views/qlkhhd/v_qlkhhd.php";
		include("public/include/layout.php");
	}

	public function Hien_thi_hoa_don($ma_khach_hang)
	{
		//Model
		$m_qlkhhd = new M_qlkhhd();
		$hds = $m_qlkhhd->Doc_hoa_don($ma_khach_hang);
		$ten_khach_hang = $m_qlkhhd->Doc_khach_hang_theo_ma($ma_khach_hang);
		//View
		include("app/views/qlkhhd/v_hoa_don.php");
	}

	public function Hien_thi_chi_tiet_hoa_don($ma_hoa_don)
	{
		//Model
		$m_qlkhhd = new M_qlkhhd();
		$cthds = $m_qlkhhd->Doc_hoa_don_theo_ma($ma_hoa_don);

		//View
		include("app/views/qlkhhd/v_cthd.php");
	}

	public function Hien_thi_khach_hang_theo_ma($ma_khach_hang)
	{
		//Model
		$m_qlkhhd = new M_qlkhhd();
		$kh = $m_qlkhhd->Doc_khach_hang_theo_ma($ma_khach_hang);

		//View
		include("app/views/qlkhhd/v_khach_hang.php");
	}
}


?>