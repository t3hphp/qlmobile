<?php 
include_once("../config/database.php");
class M_nha_sx extends database
{
	public function Doc_nha_sx()
	{
		$sql = "select * from nha_san_xuat";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	//`ma_nha_san_xuat`, `ten_nha_san_xuat`, `logo`
	public function Them_nha_sx($ten_nha_san_xuat, $logo)
	{
		$sql = "insert into nha_san_xuat values(?, ?, ?)";
		$this->setQuery($sql);
		$param = array(NULL,$ten_nha_san_xuat,$logo);
		return $this->execute($param);
	}

	public function Xoa_nha_sx($ma_nha_san_xuat)
	{
		$sql = "delete from nha_san_xuat where ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_nha_san_xuat));
	}

	public function Sua_nha_sx($ten_nha_san_xuat, $logo, $ma_nha_san_xuat)
	{
		$sql = "update nha_san_xuat set ten_nha_san_xuat = ?, logo = ? where ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->execute(array($ten_nha_san_xuat, $logo, $ma_nha_san_xuat));
	}

	public function Doc_nha_sx_theo_ma($ma_nha_san_xuat)
	{
		$sql = "select * from nha_san_xuat where ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_nha_san_xuat));
	}

	public function Tim_nha_sx($ten_nha_san_xuat)
	{
		$sql = "select * from nha_san_xuat where ten_nha_san_xuat like '%" . $ten_nha_san_xuat . "%'";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}

?>