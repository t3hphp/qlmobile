<?php 
include_once("../config/database.php");
class M_chart extends database
{
	public function Dem_dien_thoai()
	{
		$sql = "select count(*) from dien_thoai";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Dem_nha_san_xuat()
	{
		$sql = "select count(*) from nha_san_xuat";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Dem_khach_hang()
	{
		$sql = "select count(*) from user";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Dem_hoa_don()
	{
		$sql = "select count(*) from hoa_don";
		$this->setQuery($sql);
		return $this->loadRecord();
	}
}
 ?>
