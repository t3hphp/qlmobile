<?php 
include("../config/database.php");
class M_qlkhhd extends database
{
	public function Doc_khach_hang()
	{
		$sql = "select * from user";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_hoa_don($ma_khach_hang)
	{
		$sql = "select * from hoa_don where ma_khach_hang = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_khach_hang));
	}

	public function Doc_khach_hang_theo_ma($ma_khach_hang)
	{
		$sql = "select * from user where ma_khach_hang = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_khach_hang));
	}

	public function Doc_hoa_don_theo_ma($ma_hoa_don)
	{
		$sql = "select chi_tiet_hoa_don.*, dien_thoai.ten_dien_thoai from chi_tiet_hoa_don, dien_thoai where chi_tiet_hoa_don.ma_dien_thoai = dien_thoai.ma_dien_thoai and ma_hoa_don = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_hoa_don));
	}

	public function Tim_khach_hang($ho_ten)
	{
		$sql = "select * from user where ho_ten like '%" . $ho_ten . "%'";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Cap_nhat_tinh_trang_hoa_don($ma_hoa_don, $tinh_trang)
	{
		$sql = "update hoa_don set trang_thai = ? where ma_hoa_don = ?";
		$this->setQuery($sql);
		return $this->execute(array($tinh_trang, $ma_hoa_don));
	}
}

 ?>