<?php 
include_once("../config/database.php");
class M_dien_thoai extends database
{
	public function Doc_dien_thoai($vt=-1, $limit=-1)
	{
		$sql = "select dt.*, nsx.ten_nha_san_xuat from dien_thoai dt, nha_san_xuat nsx where dt.ma_nha_san_xuat = nsx.ma_nha_san_xuat order by ngay_cap_nhat desc";
		if($vt>=0 && $limit>0)
		{
			$sql .=" limit $vt,$limit";
		}
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_dien_thoai_theo_ma($ma_dien_thoai)
	{
		$sql = "select * from dien_thoai where ma_dien_thoai = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_dien_thoai));
	}

	//== Hàm thêm:
	// - Không truyền khóa chính
	// - Nhưng câu sql vẫn truyền, giá trị là null
	public function Them_dien_thoai($ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, $so_luot_xem, $so_luot_mua, $ngay_cap_nhat, $bao_hanh)
	{
		$sql = "INSERT into dien_thoai values(?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array(NULL, $ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, $so_luot_xem, $so_luot_mua, $ngay_cap_nhat, $bao_hanh));
	}
	
	//== Hàm sửa:
	// - Nhớ có mã điện thoại ở phía cuối
	// - Viết thêm hàm Đọc đt theo mã để load dữ liệu lên
	public function Sua_dien_thoai($ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, $so_luot_xem, $so_luot_mua, $ngay_cap_nhat, $bao_hanh, $ma_dien_thoai)
	{
		$sql = "UPDATE dien_thoai set ma_nha_san_xuat=?, ten_dien_thoai=?, mo_ta=?, tom_tat_thong_so=?, hinh=?, don_gia=?, don_gia_khuyen_mai=?, so_luot_xem=?, so_luot_mua=?, ngay_cap_nhat=?, bao_hanh=? where ma_dien_thoai = ?";
		$this->setQuery($sql);
		$param = array($ma_nha_san_xuat, $ten_dien_thoai, $mo_ta, $tom_tat_thong_so, $hinh, $don_gia, $don_gia_khuyen_mai, $so_luot_xem, $so_luot_mua, $ngay_cap_nhat, $bao_hanh, $ma_dien_thoai);
		return $this->execute($param);
	}

	public function Xoa_dien_thoai($ma_dien_thoai)
	{
		$sql = "DELETE from dien_thoai where ma_dien_thoai = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_dien_thoai));
	}

	public function Tim_dien_thoai($ten_dien_thoai)
	{
		//select dt.*, nsx.ten_nha_san_xuat from dien_thoai dt, nha_san_xuat nsx where dt.ma_nha_san_xuat = nsx.ma_nha_san_xuat
		$sql = "select dt.*, nsx.ten_nha_san_xuat from dien_thoai dt, nha_san_xuat nsx where dt.ma_nha_san_xuat = nsx.ma_nha_san_xuat and ten_dien_thoai like '%" . $ten_dien_thoai . "%'";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}
//ma_dien_thoai, ma_nha_san_xuat, ten_dien_thoai, mo_ta, tom_tat_thong_so, hinh, don_gia, don_gia_khuyen_mai, so_luot_xem, so_luot_mua, ngay_cap_nhat, bao_hanh, con_hang

/*$m = new M_dien_thoai();
$a = $m->Doc_dien_thoai();
echo count($a);*/
?>