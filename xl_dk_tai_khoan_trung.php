<?php 
//Tìm tài khoản trùng
$tk = $_POST["tk"];
if($tk == "")
{
	echo '';
}
elseif(strlen($tk) < 6)
{
	echo 'Tài khoản phải có ít nhất 6 ký tự';
}
else
{
	if(preg_match("/[^a-zA-Z0-9]/", "$tk"))
	{
		echo 'Tài khoản chỉ được gồm chữ và số, không được có ký tự đặc biệt';
	}
	else
	{
		include("app/models/m_khach_hang.php");
		$m_khach_hang = new M_khach_hang();
		$tktrung = $m_khach_hang->Doc_tai_khoan_theo_ten_tai_khoan($tk);
		if(!$tktrung)
		{
			echo 'Bạn có thể sử dụng tên tài khoản này';
		}
		elseif($tk == $tktrung->tai_khoan)
		{
			echo 'Tài khoản này đã được đăng ký';
		}
	}
}
?>


