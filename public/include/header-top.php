<div class="header-top-w3layouts hidden-print">
	<div class="container">
		<div class="col-md-6 logo-w3">
			<a href="."><img src="images/logo/ql-mobile.png" alt="QLMobile" /></a>
		</div>
		<div class="col-md-6 phone-w3l">
			<?php if(isset($_SESSION["khach_hang"])) 
			{
				echo
				'<div class="btn-group">
				<button type="button" class="btn btn-default">' . 'Xin chào, <strong>' .$_SESSION["khach_hang"] . '</strong></button>
				<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="thong-tin.php">Thông tin & đơn hàng</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="logout.php">Đăng xuất <i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
				</ul>
			</div>';
		}
		else
		{
			echo
			'<ul>
			<li><a href="dang-nhap.php" type="button" class="btn btn-success">Đăng nhập <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
			<li><a href="dang-ky.php" class="btn btn-primary">Đăng ký <i class="fa fa-unlock" aria-hidden="true"></i></a></li>
		</ul>';
	}
	?>
</div>
<div class="clearfix"></div>
</div>
</div>