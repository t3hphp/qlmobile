<div class="banner-agile">
	<div class="container">
		<div style="padding:40px 0">
			<h2 id="animate1">Chào mừng bạn đến với</h2>
			<h3 id="animate2">QLMobile</h3>
			<p id="animate3">Hệ thống cửa hàng kinh doanh điện thoại và phụ kiện với giá cả cạnh tranh nhất ở Sài Gòn! Chúng tôi luôn cung cấp những sản phẩm tốt nhất cho quý khách hàng.</p>
			<a id="animate4" href="lien-he.php">Liên hệ <i class="fa fa-caret-right" aria-hidden="true"></i>
			</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function()
	{
		$("#animate1").addClass('animated bounceInLeft');
		$("#animate2").addClass('animated zoomInRight');
		$("#animate3").addClass('animated flipInX');
		$("#animate4").addClass('animated tada');
	})
</script>