<!DOCTYPE html>
<html lang="en">

<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php include("head.php") ?>

<body>
	<?php include("header-top.php") ?>
	<?php include("header-bottom.php") ?>
	<div class="content">
		<div class="container">
			<?php include("content.php") ?>
		</div>
	</div>
	<?php include("footer.php") ?>

</body>
</html>