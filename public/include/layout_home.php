<!DOCTYPE html>
<html lang="en">

<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php include("head.php") ?>

<body>
	<?php include("header-top.php") ?>
	<?php include("header-bottom.php") ?>
	<?php include("banner.php") ?>
	<?php include("top-product.php") ?>
	<?php include("service.php") ?>
	<?php include("brands.php") ?>
	<?php include("footer.php") ?>
</body>
</html>