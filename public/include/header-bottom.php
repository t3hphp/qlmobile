<?php 
include_once("app/models/m_nha_san_xuat.php");
$m_nha_san_xuat = new M_nha_san_xuat();
$nsx07s = $m_nha_san_xuat->Doc_ten_nha_san_xuat_07();
$nsx7es = $m_nha_san_xuat->Doc_ten_nha_san_xuat_7end();
?>
<div class="header-bottom-w3ls hidden-print">
	<div class="container">
		<div class="col-md-7 navigation-agileits">
			<nav class="navbar navbar-default">
				<div class="navbar-header nav_2">
					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div> 
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav ">
						<?php 
						 $urlArr = explode('/', $_SERVER['REQUEST_URI']);
						$thisUrl = $urlArr[2]; 
						$loai_dt = explode('?', $thisUrl);
						?> 

						<li class="<?php echo ($thisUrl=="")?"active":"" ?>"><a href="." class="hyper "><span>Trang chủ</span></a></li>	

						<li class="dropdown <?php echo ($loai_dt[0]=="loai-dien-thoai.php")?"active":"" ?>">
							<a href="#" class="dropdown-toggle  hyper" data-toggle="dropdown" ><span>Điện thoại<b class="caret"></b></span></a>
							<ul class="dropdown-menu multi">
								<div class="row">
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<?php foreach($nsx07s as $n){ ?>
											<li><a href="loai-dien-thoai.php?nsx=<?php echo $n->ma_nha_san_xuat ?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?php echo $n->ten_nha_san_xuat ?></a></li>
											<?php } ?>
										</ul>
									</div>
									<div class="col-sm-3">
										<ul class="multi-column-dropdown">
											<?php foreach($nsx7es as $n){ ?>
											<li><a href="loai-dien-thoai.php?nsx=<?php echo $n->ma_nha_san_xuat ?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?php echo $n->ten_nha_san_xuat ?></a></li>
											<?php } ?>
										</ul>						
									</div>
									<div class="col-sm-4 w3l">
										<a href="loai-dien-thoai.php" class="hyper"><img src="images/smartphone.png" class="img-responsive" alt="Xem tất cả"></a>
									</div>
									<div class="clearfix"></div>
								</div>	
							</ul>
						</li>

						<li class=" <?php echo ($thisUrl=="lien-he.php")?"active":"" ?>"><a href="lien-he.php" class="hyper"><span>Liên hệ</span></a></li>

						<li class=" <?php echo ($thisUrl=="huong-dan-mua-hang.php")?"active":"" ?>"><a href="huong-dan-mua-hang.php" class="hyper"><span>Hướng dẫn mua hàng</span></a></li>
					</ul>
				</div>
			</nav>
		</div>
		<script>
			$(document).ready(function(){
				$(".dropdown").hover(            
					function() {
						$('.dropdown-menu', this).stop( true, true ).slideDown("fast");
						$(this).toggleClass('open');        
					},
					function() {
						$('.dropdown-menu', this).stop( true, true ).slideUp("fast");
						$(this).toggleClass('open');       
					}
					);
			});
		</script>
		<div class="col-md-4 search-agileinfo">
			<form action="search.php?q" method="get">
				<input type="search" name="q" placeholder="Tìm kiếm sản phẩm..." required="">
				<button type="submit" class="btn btn-default search" aria-label="Left Align">
					<i class="fa fa-search" aria-hidden="true"> </i>
				</button>
			</form>
		</div>
		<div class="col-md-1 cart-wthree">
			<?php include_once("app/controllers/c_gio_hang.php");
			$c_gio_hang = new C_gio_hang();
			$c_gio_hang->Shopping_cart_icon() ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

