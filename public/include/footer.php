<!--đăng kí nhận tin-->
<?php include("app/controllers/c_dang_ky_nhan_tin.php");
$c_dang_ky_nhan_tin = new C_dang_ky_nhan_tin();
$c_dang_ky_nhan_tin->Hien_thi_dang_ky_nhan_tin();
?>
<!-- footer -->
<br/>
<div class="footer hidden-print">
	<div class="container">
		<div class="col-md-3 footer-grids ">
			<a href="."><img src="images/logo/ql-mobile.png" alt="QLMobile" /></a>
			<ul>
				<li>357 Lê Hồng Phong P.2 Q.10</li>
				<li>Thành phố Hồ Chí Minh.</li>
				<li><a href="mailto:qlmobile.t3h@gmail.com">qlmobile.t3h@gmail.com</a></li>
			</ul>
		</div>
		<div class="col-md-3 footer-grids fgd1 fgd2">
			<h4>Kết nối chúng tôi</h4> 
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			<hr/>
			<div class="fb-like" data-href="http://localhost/QLMobile" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
		</div>
		<div class="col-md-3 footer-grids fgd3">	
			<h4>Học viên thực hiện</h4> 
			<ul>
				<li><a href="https://www.facebook.com/tvqqq">Tất Vĩ Quyền</a> - <small>tvqisme@gmail.com</small></li>
				<li><a href="admin" style="background-color: #fff; padding:10px 0px; color:#ededed">Admin CMS</a></li>
			</ul>
		</div>
		<div class="col-md-3 footer-grids fgd4">
			<h4>My Account</h4> 
			<ul>
				<li><a href="dang-nhap.php">Đăng nhập</a></li>
				<li><a href="dang-ky.php">Đăng ký</a></li>
				<li><a href="gio-hang.php">Giỏ hàng </a></li>
			</ul>
		</div>
		<div class="clearfix"></div>
		<p class="copy-right">© 04/2017 <strong>QLMobile</strong> | Đồ án PHP - Khóa 227T26 - Trung Tâm Tin Học - ĐH KHTN | Template: Fashion Club. Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
	</div>
</div>
<hr class="colorgraph" style="height: 17px;padding: 0;margin: 10px 0 0;"/>

<!--back to top-->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Trở về đầu trang" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

<style type="text/css">
	.back-to-top {
		cursor: pointer;
		position: fixed;
		bottom: 50px;
		right: 20px;
		display:none;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
        	$('#back-to-top').tooltip('hide');
        	$('body,html').animate({
        		scrollTop: 0
        	}, 800);
        	return false;
        });
        
        $('#back-to-top').tooltip('show');

    });
</script>
<!--end back to top-->

<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '101718183721736',
			xfbml      : true,
			version    : 'v2.8'
		});
		FB.AppEvents.logPageView();
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!--pure chat-->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'b65ddddd-17d6-4ab5-953b-d8f3e99022b1', f: true }); done = true; } }; })();</script>