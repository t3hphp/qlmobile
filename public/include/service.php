<div class="fandt">
	<div class="container">
		<div class="col-md-6 features">
			<h3>Phong cách của chúng tôi</h3>
			<div class="support">
				<div class="col-md-2 ficon hvr-rectangle-out">
					<i class="fa fa-user " aria-hidden="true"></i>
				</div>
				<div class="col-md-10 ftext">
					<h4>Hỗ trợ 24/7 và 365 ngày/năm</h4>
					<p>Khi bạn cần, chúng tôi có.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="shipping">
				<div class="col-md-2 ficon hvr-rectangle-out">
					<i class="fa fa-bus" aria-hidden="true"></i>
				</div>
				<div class="col-md-10 ftext">
					<h4>Miễn phí giao hàng</h4>
					<p>Không có một chi phí phát sinh nào khác.</p>
				</div>	
				<div class="clearfix"></div>
			</div>
			<div class="money-back">
				<div class="col-md-2 ficon hvr-rectangle-out">
					<i class="fa fa-money" aria-hidden="true"></i>
				</div>
				<div class="col-md-10 ftext">
					<h4>Hoàn tiền 100%</h4>
					<p>Nếu bạn không vừa ý, hãy liên hệ chúng tôi để nhận lại tiền.</p>
				</div>	
				<div class="clearfix"></div>				
			</div>
		</div>
		<div class="col-md-6 testimonials">
			<div class="test-inner">
				<div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
					<div class="wmuSliderWrapper">
						<article style="position: absolute; width: 100%; opacity: 0;"> 
							<div class="banner-wrap">
								<img src="images/logo/tvq.jpg" alt="TVQ" class="img-responsive" />
								<p>Tôi đã mua được một chiếc điện thoại tuyệt vời ở QLMobile với giá không tưởng. Triệu like cho QLMobile!</p>
								<h4># TVQ</h4>
							</div>
						</article>
						<article style="position: absolute; width: 100%; opacity: 0;"> 
							<div class="banner-wrap">
								<img src="images/logo/long.jpg" alt="PikaLong" class="img-responsive" />
								<p>Không thể tin nổi, thật không thể tin được có một cửa hàng điện thoại xem khách hàng hơn cả thượng đế. Pika! Pika!.</p>
								<h4># PikaLong</h4>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<script src="public/layout/js/jquery.wmuSlider.js"></script> 
	<script>
		$('.example1').wmuSlider();         
	</script> 
</div>