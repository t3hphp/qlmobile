<head>
	<?php @session_start() ?>
	<title><?php echo (isset($title)?$title:"QLMobile") ?></title>
	<link rel="shortcut icon" type="image/png" href="images/logo/ql-favicon-s.png"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="QLMobile | Đồ án môn lập trình PHP - Trung Tâm Tin Học - ĐH KHTN" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- css -->
	<link href="public/layout/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="public/layout/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="public/layout/css/font-awesome.min.css" type="text/css" media="all" />

	<link rel="stylesheet" href="public/layout/css/cssplus.css" type="text/css" media="all" />
	<!--// css -->

	<!-- font -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- //font -->

	<script src="public/layout/js/jquery-1.11.1.min.js"></script>
	<script src="public/layout/js/bootstrap.js"></script>

	<script src="public/layout/js/jsplus.js"></script>
	<script src="public/layout/js/thu_vien_ajax.js"></script>
	<script src="public/layout/js/kiemtra.js"></script>
	<script src="public/layout/js/ajax_cart.js"></script>

	<!--shorten jquery-->
	<script src="public/layout/js/jquery.shorten.js"></script>

	<!--sweet alert-->
	<script src="public/layout/swal2/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="public/layout/swal2/sweetalert2.min.css">

	<!--animate-->
	<link rel="stylesheet" href="public/layout/css/animate.css">

	<!--flex slider-->
	<script defer src="public/layout/js/jquery.flexslider.js"></script>
	<link rel="stylesheet" href="public/layout/css/flexslider.css" type="text/css" media="screen" />
	<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
</script>
<!--flex slider-->

<script src="public/layout/js/imagezoom.js"></script>
<!--image zoom product-->

<?php include("config/curPage.php") ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=101718183721736";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>