<?php 
include_once("app/models/m_nha_san_xuat.php");
$m_nha_san_xuat = new M_nha_san_xuat();
$nsxs = $m_nha_san_xuat->Doc_ten_nha_san_xuat();
?>
<div class="top-brands">
	<div class="container">
		<h3>Brands</h3>
		<div class="sliderfig">
			<ul id="flexiselDemo1">
				<?php foreach($nsxs as $nsx){ ?>
				<li>
					<img src="images/brand/<?php echo $nsx->logo ?>" alt="<?php echo $nsx->ten_nha_san_xuat ?>" class="img-responsive" />
				</li>
				<?php } ?>
			</ul>
		</div>
		<script type="text/javascript">
			$(window).load(function() {
				$("#flexiselDemo1").flexisel({
					visibleItems: 4,
					animationSpeed: 1000,
					autoPlay: true,
					autoPlaySpeed: 3000,    		
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: { 
						portrait: { 
							changePoint:480,
							visibleItems: 1
						}, 
						landscape: { 
							changePoint:640,
							visibleItems:2
						},
						tablet: { 
							changePoint:768,
							visibleItems: 3
						}
					}
				});

			});
		</script>
		<script type="text/javascript" src="public/layout/js/jquery.flexisel.js"></script>
	</div>
</div>