$(function(){
	$('.w3ls-cart').click(function(){
		var key = $(this).attr('id');
		var sosanpham = $("#sosanpham" + key).val();
		var dongia = $("#dongia" + key).val();
		
		var data = {
			id: key,
			sosanpham: sosanpham,
			dongia: dongia
		};

		$.ajax({
			url: 'xl_mua_hang.php',
			type: 'POST',
			async: false,
			data: data,
			//dataType: 'json',
			success: function(dataBack){
				swal({
					text: dataBack,
					timer: 1500,
					customClass: 'animated tada',
				});
				setTimeout(function(){ location.reload(true)}, 1500);
				// alert('SL đặt: ' + dataBack['sl'] + ' - Tổng tiền: ' + dataBack['tc'] );
			},
			error: function(error){
				alert(error.statusText);
			}
		}); //end ajax
		return false;
	}); //end click
}); //end function

  //Định dạng số
  function formatCurrency(num) 
  {
  	var num = num.toString().replace(/\$|\,/g,'');
  	if(isNaN(num))
  		num = "0";
  	sign = (num == (num = Math.abs(num)));
  	num = Math.floor(num*100+0.50000000001);
  	num = Math.floor(num/100).toString();
  	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
  		num = num.substring(0,num.length-(4*i+3))+','+
  	num.substring(num.length-(4*i+3));
  	return (((sign)?'':'-') + num);
  }