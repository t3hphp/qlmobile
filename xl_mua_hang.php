<?php 
@session_start();
include_once("app/controllers/c_gio_hang.php");
$c_gio_hang = new C_gio_hang();
$key = $_POST['id'];
$sosanpham = ((int)$_POST['sosanpham']);
$dongia = ((double)$_POST['dongia']);

if($sosanpham >= 0 && $dongia > 0)
{
	$c_gio_hang->Them_gio_hang($key, $sosanpham, $dongia);
}

include_once("app/models/m_dien_thoai.php");
$m_dien_thoai = new M_dien_thoai();
$dt = $m_dien_thoai->Doc_dien_thoai($key);

if(isset($_SESSION['cart']['da_co_sp_nay']))
{
	echo 'Đã có sản phẩm này trong giỏ hàng!';
	unset($_SESSION['cart']['da_co_sp_nay']);
}
else
{
	echo 'Đã thêm ' . $dt->ten_dien_thoai . ' vào giỏ hàng của bạn!';
}
 ?>

