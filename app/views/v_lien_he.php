<?php 
@session_start();

//====Code PHP - Gửi mail======
//Hàm kiểm tra mail hợp lệ
function spamcheck($mail)
{
	//The FILTER_SANITIZE_EMAIL filter removes all illegal e-mail characters from a string.
	//This filter allows all letters, digits and $-_.+!*'{}|^~[]`#%/?@&=
	$mail=filter_var($mail,FILTER_SANITIZE_EMAIL);
	if(filter_var($mail,FILTER_VALIDATE_EMAIL))
	{
		return true;	
	}
	else
	{
		return false;	
	}
}

//Tạo Captcha
function taocaptcha()
{
	if(file_exists("images/captcha.jpg"))
		unlink("images/captcha.jpg");
	$captcha = imagecreatefromjpeg('images/pattern.jpg');
	//set some variables
	$red = imagecolorallocate($captcha, 225, 0, 0);
	$font = 'public/layout/fonts/captcha1.ttf';
	//random stuff
	$string = md5(microtime() * time());
	$text = substr($string, 0, 5);
	$_SESSION['code'] = $text;
	//create some stupid stuff
	imagettftext($captcha, 18, 2, 11, 22, $red, $font, $text);
	// begin to create image
	//header('content-type: image/png');
	imagepng($captcha,"images/captcha.jpg");
	//clean up
	imagedestroy($captcha);
}

//Gửi mail
if(!isset($_POST["btn_gui_mail"]))
{
	taocaptcha();
}
if(isset($_POST['btn_gui_mail']))
{
	require_once("public/layout/smtpgmail/class.phpmailer.php");
	$mail=new PHPMailer();
	$mail->IsSMTP(); // Chứng thực SMTP
	$mail->SMTPAuth=TRUE;
	$mail->Host="smtp.gmail.com";
	$mail->Port=465;
	$mail->SMTPSecure="ssl";
	/* Server google*/
	$mail->Username="qlmobile.t3h@gmail.com"; // Nhập mail
	$mail->Password="quyen1996"; // Mật khẩu
	/* Server google*/
	$mail->CharSet="utf-8";
	$noidung="Họ tên: " .$_POST["your_name"]."<br/>";
	$noidung .="Email: " .$_POST["your_email"];

	$noidung .="<hr><br>Chủ đề: " .$_POST["your_subject"];
	$noidung .="<br>Nội dung: " .$_POST["your_message"];

	$mail->SetFrom($_POST["your_email"],$_POST["your_name"]);
	$mail->Subject=$_POST["your_subject"];

	$mail->MsgHTML($noidung);

	$mail->AddAddress("qlmobile.t3h@gmail.com","Quản Trị"); // Mail người nhận
	/*$path="images/captcha.gif";
	$mail->AddAttachment($path,"captcha.gif");
	if($_POST["chk"]==1)
	{
		$mail->AddBCC($_POST["th_email"],"Name");
	}*/
	if($_POST["security_code"]==$_SESSION["code"])
	{
		if(spamcheck($_POST["your_email"]))
		{
			if(!$mail->Send())
			{
				$err = "Mail lỗi".$mail->ErrorInfo;
			}
			else
			{
				$err = "Gửi mail thành công !!!";
			}
		}
		else
		{
			$err = "Địa chỉ mail không hợp lệ.";
		}
	}
	else
	{
		$err = "Nhập mã bảo vệ...";	
	}
}
?>

<div class="contact">
	<div class="container">
		<h3>Liên hệ</h3>
		<div class="col-md-3 col-sm-3 contact-left">
			<div class="address">
				<h4>ĐỊA CHỈ</h4>
				<h5>357 Lê Hồng Phong P.2 Q.10,</h5>
				<h5>Thành phố Hồ Chí Minh.</h5>
			</div>
			<hr/>
			<div class="email">
				<h4>EMAIL</h4>
				<h5><a href="mailto:qlmobile.t3h@gmail.com">qlmobile.t3h@gmail.com</a></h5>
				<h5><a href="mailto:tvqisme@gmail.com">tvqisme@gmail.com</a></h5>
			</div>
		</div>
		<div class="col-md-9 col-sm-9 contact-right">
			<form action="#" method="post">
				<input type="text" name="your_name" placeholder="Tên bạn" class="kiemtra" data_error="Nhập tên của bạn">
				<input type="text" id="email" name="your_email" placeholder="Email" class="kiemtra" data_error="Nhập email của bạn">
				<input type="text" name="your_subject" placeholder="Tiêu đề" style="width:96%" class="kiemtra" data_error="Nhập tiêu đề">
				<textarea name="your_message" placeholder="Nội dung" class="kiemtra" data_error="Nhập nội dung"></textarea>
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
							<label>Nhập mã bảo vệ: </label>
							<img src="images/captcha.jpg" alt="captcha" style="width:100px!important" />
							<input type="image" name="th_recapcha" src="images/recaptcha.png" style="width:20px;" value="recapcha"  />
						</div>
						<div class="col-md-4">
							<input type="text" id="security_code" name="security_code" class="kiemtra" data_error="Nhập mã bảo vệ" style="width:100px !important; background-color:#DDD; border-style:none" />	
						</div>
					</div>

					<div class="col-md-3">
						<input class="pull-right" type="submit" value="Gửi" name="btn_gui_mail" id="gui_mail" onclick="return Kiemtradulieu()">
					</div>
				</div>
				<p class="text-warning"><?php echo isset($err)?$err:"" ?></p>
			</form>
		</div>
	</div>
</div>
<hr class="colorgraph">
<div class="map-w3ls">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.629034633842!2d106.67994971422614!3d10.763046692330612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f1eaacae6dd%3A0xeaf52859c6ff873c!2zVHJ1bmcgdMOibSBUaW4gaOG7jWMgLSDEkOG6oWkgaOG7jWMgS2hvYSBo4buNYyBU4buxIG5oacOqbg!5e0!3m2!1sen!2s!4v1492337857997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<script type="text/javascript">
	$(function()
	{
		$('#gui_mail').click(function()
		{
			swal({"Cảm ơn bạn đã liên hệ chúng tôi!"});
		})
	})
</script>