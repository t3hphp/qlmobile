<?php 
if($flag == 0)
{
	echo '<h2>Không tìm thấy sản phẩm nào!</h2>';
}
else
{
	?>
	<div class="container">
		<div class="row">
			<h3 class="text-primary">Có <?php echo count($dts) ?> sản phẩm với từ khóa "<?php echo $_GET["q"] ?>"</h3>
			<div class="col-md-12 women-dresses">
				<?php 
				$count = 1;
				foreach($dts as $dt) { ?>
				<div class="col-md-3 women-grids wp1 animated wow slideInUp" data-wow-delay=".5s">
					<a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><div class="product-img">
						<img src="images/<?php echo $dt->hinh ?>" alt="" />
						<div class="p-mask">
							<button type="submit" class="w3ls-cart pw3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Mua ngay</button>
						</div>
					</div></a>
					<h4><a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><?php echo $dt->ten_dien_thoai ?></a></h4>
					<h5><?php 
						if($dt->don_gia_khuyen_mai>1)
						{
							echo '<del>' . number_format($dt->don_gia) .' VNĐ' . '</del>' . '<div class="dgkm">' . number_format($dt->don_gia_khuyen_mai) . ' VNĐ' . '</div>';
						}
						else
						{
							echo number_format($dt->don_gia) .' VNĐ';
						}
						?>
					</h5>
				</div>
				<?php
				if($count % 4 == 0){	echo '<div class="clearfix"></div>';	}
				$count++;
			} ?>
			<br/>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
</div>
<?php } ?>

<div class="row text-center" style="padding:20px">
	<a href="loai-dien-thoai.php" class="btn btn-lg btn-default">Xem thêm các sản phẩm khác</a>
</div>
