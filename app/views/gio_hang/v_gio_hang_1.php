<h2><strong>Giỏ hàng</strong> của bạn</h2>
<hr class="colorgraph" style="width:400px"> 
<?php
if(!isset($_SESSION['cart']))
{
    echo '<h3>Giỏ hàng rỗng</h3>';
} 
else
{
    $tien_tong_cong = 0;
    ?>
    <table class="table table-striped" style="width:100%; border: none; margin-bottom:0">
        <thead class="gio-hang">
            <tr>
                <th width="10%"></th>
                <th width="35%">Sản phẩm</th>
                <th width="10%" class="text-center">SL</th>
                <th width="15%" class="text-right">Đơn giá <p style="font-size:11px; letter-spacing: 0">(đã tính KM nếu có)</p></th>
                <th width="15%" class="text-right">Thành tiền</th>
                <th width="5%" class="text-right" style="color:#FF3D00">Xóa</th>
            </tr>
        </thead>
        <tbody>
            <form method="post">
                <?php foreach($ds_dien_thoai_gio_hang as $item) { ?>
                <tr>
                    <td><img src="images/<?php echo $item->hinh ?>" style="width: 80px!important"/></td>
                    <td style="font-size:16px; font-weight: bold"><?php echo $item->ten_dien_thoai ?></td>

                    <!--số lượng-->
                    <td><input class="form-control" type="number" min="1" width="80%" name="soluong<?php echo $item->ma_dien_thoai ?>"" value="<?php echo $item->sl ?>"/></td>
                    <input type="hidden" name="dongia<?php echo $item->ma_dien_thoai ?>" value="<?php echo ($item->don_gia_khuyen_mai>1)?$item->don_gia_khuyen_mai:$item->don_gia ?>" />

                    <td class="text-right"><?php echo number_format(($item->don_gia_khuyen_mai>1)?$item->don_gia_khuyen_mai:$item->don_gia) ?> VNĐ</td>

                    <td class="text-right"><?php echo number_format(($item->don_gia_khuyen_mai>1) ? $item->sl * $item->don_gia_khuyen_mai : $item->sl * $item->don_gia) ?> VNĐ</td>

                    <td class="text-right"><input type="checkbox" name="<?php echo $item->ma_dien_thoai ?>" value="<?php echo ($item->don_gia_khuyen_mai>1)?$item->don_gia_khuyen_mai:$item->don_gia ?>"/></td>
                </tr>
                <?php $tien_tong_cong += (($item->don_gia_khuyen_mai>1) ? $item->sl * $item->don_gia_khuyen_mai : $item->sl * $item->don_gia); ?>
                <?php } ?>
            </tbody>
        </table>
        <div class="row tong-cong flex">
            <div class="col-md-7">
                <a href="loai-dien-thoai.php" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i> Tiếp tục mua sắm</a>
                <input type="submit" name="btnCapNhat" class="btn btn-warning" value="Cập nhật / Xóa" />
            </div>
            <div class="col-md-2">
                <strong>Tổng cộng:</strong>
            </div>
            <div class="col-md-3">
                <p class="gia-tien"><?php echo number_format($tien_tong_cong) ?> VNĐ</p>
            </div>
        </div>
    </form>
    <br/>
    <div class="col-md-2 col-md-offset-10">
        <a href="xac-thuc.php" class="btn btn-lg btn-success" style="font-weight: bold">ĐẶT HÀNG <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
    </div>
    <div class="clearfix"></div>

    <style>
        input[type="radio"], input[type="checkbox"]{
            height: 17px;
            width: 35px;
        }
        .gia-tien
        {
            font-size: 22px;
            font-weight: bold;
            color: #BF360C;
        }
        .tong-cong{
            padding: 20px 0;
            background-color: #FFE0B2;
        }
        thead.gio-hang
        {
            font-weight: bold;
            font-size: 18px;
            letter-spacing: 1px;
            border-top: 2px solid #c3c3c3;
            border-bottom: 2px solid #c3c3c3;
        }
    </style>
    <?php } //end else ?>