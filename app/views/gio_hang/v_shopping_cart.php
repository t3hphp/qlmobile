<div class="cart"> 
	<button id="cart" class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
	<?php if(isset($_SESSION['amount'])){ ?>
	<span class="badge" id="sl-gio-hang"><?php echo $_SESSION['amount'] ?></span>
	<?php } ?>
</div>

<?php if(isset($_SESSION['cart'])){ ?>
<!--shopping cart-->
<div class="shopping-cart" style="display: none">
	<div class="shopping-cart-header">
		<p class="badge"><?php echo $_SESSION['amount'] ?> sản phẩm</p>
		<div class="shopping-cart-total">
			<span class="lighter-text">Tổng cộng:</span>
			<span class="main-color-text"><?php echo number_format($_SESSION['total']) ?> VNĐ</span>
		</div>
	</div> <!--end shopping-cart-header -->

	<ul class="shopping-cart-items" id="scroll-bar-style-7">
		<?php foreach($ds_dien_thoai_gio_hang as $item) { ?>
		<li class="clearfix">
			<img src="images/<?php echo $item->hinh ?>" alt="<?php echo $item->hinh ?>" style="width:60px!important" />
			<span class="item-name"><?php echo $item->ten_dien_thoai ?></span>
			<span class="item-price"><?php echo number_format(($item->don_gia_khuyen_mai>1) ? $item->don_gia_khuyen_mai : $item->don_gia) ?> VNĐ</span>
			<span class="item-quantity">Số lượng: <?php echo $item->sl ?></span>
		</li>
		<?php } ?>
	</ul>

	<a href="gio-hang.php" class="button">Checkout</a>
</div> <!--end shopping-cart -->
<?php } ?>

<style>
	#scroll-bar-style-7::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
		border-radius: 10px;
	}

	#scroll-bar-style-7::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#scroll-bar-style-7::-webkit-scrollbar-thumb
	{
		border-radius: 10px;
		background-image: -webkit-gradient(linear,
			left bottom,
			left top,
			color-stop(0.44, rgb(122,153,217)),
			color-stop(0.72, rgb(73,125,189)),
			color-stop(0.86, rgb(28,58,148)));
	}


	.lighter-text {
		color: #ABB0BE;
	}
	.main-color-text {
		color: #6394F8;
	}
	.shopping-cart {
		margin-top: 15px;
		right: 21%;
		float: right;
		background: white;
		box-shadow: 2px 2px 7px #ada1ea;
		width: 350px;
		position: absolute;
		border-radius: 3px;
		padding: 10px 20px;
		z-index: 99;
	}
	.shopping-cart .shopping-cart-header {
		border-bottom: 1px solid #E8E8E8;
		padding-bottom: 7px;
	}
	.shopping-cart .shopping-cart-header .shopping-cart-total {
		float: right;
	}
	.shopping-cart .shopping-cart-items {
		padding-top: 20px;
		list-style-type: none;
		max-height: 250px;
		overflow-y: auto;
	}
	.shopping-cart .shopping-cart-items li {
		margin-bottom: 18px;
	}
	.shopping-cart .shopping-cart-items img {
		float: left;
		margin-right: 12px;
	}
	.shopping-cart .shopping-cart-items .item-name {
		display: block;
		padding-top: 10px;
		font-size: 16px;
	}
	.shopping-cart .shopping-cart-items .item-price {
		color: #6394F8;
		margin-right: 8px;
	}
	.shopping-cart .shopping-cart-items .item-quantity {
		color: #ABB0BE;
	}

	.shopping-cart:after {
		bottom: 100%;
		left: 89%;
		border: solid transparent;
		content: " ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
		border-bottom-color: white;
		border-width: 8px;
		margin-left: -8px;
	}
	.button {
		background: #6394F8;
		color: white;
		text-align: center;
		padding: 12px;
		text-decoration: none;
		display: block;
		border-radius: 3px;
		font-size: 16px;
		margin: 25px 0 15px 0;
	}
	.button:hover {
		background: #729ef9;
	}
</style>

<script type="text/javascript">
	(function(){
		$("#cart").on("click", function() {
			$(".shopping-cart").fadeToggle("fast");
		});
	})();

	$(function(){
		$('.w3view-cart').hover(function()
		{
			$(this).toggleClass('animated infinite tada');
			$('#sl-gio-hang').toggleClass('animated bounceInDown');
		});
	})
</script>