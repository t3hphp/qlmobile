<div style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="tab"></div>
		</div>
	</div>
	<div class="row" id="hoadon">
		<div class="col-sm-6 col-sm-offset-3 container-print">
			<div class="panel panel-dark panel-default pseudo">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<h3 style="margin-left:-20px"><strong>QLMobile Shop</strong></h3>
							<p>357 LHP P.2 Q.10</p>
						</div>
						<div class="col-md-6">
							<h4 class="text-right"><strong>Hóa đơn số: </strong>#<?php echo $hoa_don_moi_nhat->ma_hoa_don ?></h3>
								<p class="text-right">Ngày lập: <?php echo date('Y-m-d',strtotime($hoa_don_moi_nhat->ngay_dat_hang)) ?></p>
							</div>
						</div>
						<hr/>
						<div class="row">
							<h3 style="margin-bottom:5px">Thông tin khách hàng</h3>
							<div class="ttkh">
								<p><strong>› Họ tên:</strong> <?php echo $kh->ho_ten ?></p>
								<p><strong>› Địa chỉ:</strong> <?php echo $kh->dia_chi ?></p>
								<p><strong>› Số điện thoại:</strong> <?php echo $kh->dien_thoai ?></p>
							</div>
						</div>
						<hr/>
						<div class="row">
							<h3 style="margin-bottom:5px">Thông tin sản phẩm</h3>
							<table id="receipt" class="table table-striped" style="width:100%; border:none">
								<thead style="border-top:1px solid #eee; border-bottom:1px solid #eee">
									<tr>
										<th width="5%">STT</th>
										<th width="45%">Sản phẩm</th>
										<th width="5%">SL</th>
										<th width="25%">Đơn giá</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$stt = 1;
									foreach($cthd_moi_nhats as $cthd) { ?>
									<tr>
										<td><?php echo $stt; ?></td>
										<td><?php echo $cthd->ten_dien_thoai ?></td>
										<td><?php echo $cthd->so_luong ?></td>
										<td><?php echo number_format($cthd->don_gia) ?> VNĐ</td>
									</tr>
									<?php $stt++; 
								} ?>
							</tbody>
						</table>
					</div>
					<div class="row text-right">
						<div class="col-md-3 col-md-offset-4">Tổng cộng:</div>
						<div class="col-md-5"><strong><?php echo number_format($hoa_don_moi_nhat->tong_tien) ?> VNĐ</strong></div>
					</div>

					<hr/>
					<div class="row">
						<p>Cảm ơn bạn đã mua sắm tại QLMobile. <br/> Chúng tôi sẽ liên hệ với bạn để xác nhận đơn hàng</p>
					</div>
					<hr/>

					<div class="row text-center hidden-print">
						<button class="btn btn-default" onclick="window.print()">
							In Hóa Đơn
						</button>
						<button class="btn btn-success" id="xac-nhan">
							Xác Nhận
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
	$(function()
	{
		$('#xac-nhan').click(function(){
			swal("Hoàn tất!", "Đang chuyển về trang chủ");
			setTimeout(function(){ window.location = "."}, 1000);
		});
	});
</script>
<style type="text/css">
	table#receipt > thead > tr > th, table#receipt > tbody > tr > td{
		color: #999;
	}
	.ttkh{
		margin-left: 18px;
	}
	.tab {
		width: 90%;
		margin: 0 auto;
		height: 15px;
		margin-bottom: -5px;
		background-color: #fff;
		box-shadow: inset 0 0 35px #ddd;
		border-radius: 50px
	}

	.pseudo {
		position: relative;
	}

	.pseudo:before {
		content: "";
		position: absolute;
		width: 96%;
		top: 5px;
		left: 2%;
		border-bottom: 1px dashed #000;
	}

	.panel-dark {
		background-color: #f7f7f7;
		border-color: #f7f7f7;
		box-shadow: 0 5px 15px #ccc;
	}

	.pseudo:after {
		content: '';
		position: absolute;
		display: block;
		width: 100%;
		height: 10px;
		bottom: -10px;
		left: 0;
		background-image: linear-gradient(45deg, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%), linear-gradient(-45deg, rgba(0, 0, 0, 0) 33.333%, #f7f7f7 33.333%, #f7f7f7 66.667%, rgba(0, 0, 0, 0) 66.667%);
		background-size: 20px 40px;
		background-position: 50% -30px;
		background-repeat: repeat-x;
		z-index: 1;
	}

	.container-print {
		padding: 0;
	}

	@-webkit-keyframes shake-it-baby {
		0% {
			height: 0px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
		10% {
			height: 10px;
			transform: translate(0.5px, -0.5px) rotate(0.1deg);
		}
		20% {
			height: 20px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
		30% {
			height: 35px;
			transform: translate(0.5px, -0.5px) rotate(0.1deg);
		}
		40% {
			height: 50px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
		50% {
			height: 70px;
			transform: translate(0.5px, -0.5px) rotate(0.1deg);
		}
		60% {
			height: 95px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
		70% {
			height: 125px;
			transform: translate(0.5px, -0.5px) rotate(0.1deg);
		}
		80% {
			height: 165px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
		90% {
			height: 225px;
			transform: translate(0.5px, -0.5px) rotate(0.1deg);
		}
		100% {
			height: 280px;
			transform: translate(-0.5px, 0.5px) rotate(-0.1deg);
		}
	}

	.container-print .panel-body {
		animation: shake-it-baby 6s;
		overflow: hidden;
	}
</style>