<?php
include_once("app/controllers/c_gio_hang.php");
$c_gio_hang = new C_gio_hang();
?>

<section>
  <div class="container">
    <div class="row">
      <div class="board">
        <div class="board-inner">
          <ul class="nav nav-tabs" id="myTab">
            <div class="liner"></div>
            <li class="active">
              <a href="#" data-toggle="tab" title="Giỏ hàng">
                <span class="round-tabs one">
                  <i class="fa fa-shopping-cart fa-gio-hang animated infinite tada" style="margin-left:-3px" aria-hidden="true"></i>
                </span> 
              </a>
            </li>
            <li><a href="#" title="Xác thực">
             <span class="round-tabs two">
               <i class="fa fa-user fa-gio-hang"></i>
             </span> 
           </a>
         </li>
         <li><a href="#" title="Đơn hàng">
           <span class="round-tabs three">
             <i class="fa fa-gift fa-gio-hang"></i>
           </span> </a>
         </li>
       </ul>
     </div>

     <div class="tab-content">
      <!--1. giỏ hàng-->
      <div class="tab-pane fade in active" id="gio-hang">
        <?php $c_gio_hang->Hien_thi_gio_hang_1() ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
</div>
</section>

<link rel="stylesheet" href="public/layout/css/css_gio_hang.css" type="text/css" media="all" />
<script type="text/javascript">
  $(function(){
    $('a[title]').tooltip();
  });

</script>