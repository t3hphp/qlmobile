<!--đã đăng nhập-->
<?php if(isset($_SESSION["khach_hang"]))
{ 
	?>
	<div class="row">
		<h2>Thông tin người đặt hàng</h2>
		<hr/>
		<div class="col-md-6 col-md-offset-4">
			<p><strong>Họ và tên: </strong><?php echo $kh->ho_ten ?></p>
			<p><strong>Địa chỉ: </strong><?php echo $kh->dia_chi ?></p>
			<p><strong>Số điện thoại: </strong><?php echo $kh->dien_thoai ?></p>
		</div>
	</div>
	<br/>
	<div class="row text-center">
		<div class="tiep-tuc">
			<form method="post">
				<input type="submit" class="btn btn-lg btn-success" name="in-hoa-don" value='Tiếp tục với tư cách <?php echo $kh->ho_ten ?>'/>
			</form>
		</div>
	</div>
	<?php }
	else { ?>
	<!--chưa đăng nhập-->
	<h2>Chúng tôi cần biết bạn <strong>là ai?</strong><br/></h2>
	<br/><h4 class="text-danger">Chọn đăng nhập nếu đã có tài khoản QLMobile hoặc tạo mới một tài khoản.</h4>
	<br/>
	<div class="row">
		<div class="col-md-3 col-md-offset-3">
			<a class="btn btn-success btn-login" href="dang-nhap.php">Đăng nhập</a>
		</div>
		<div class="col-md-3">
			<a class="btn btn-info btn-login" href="dang-ky.php">Đăng ký</a>
		</div>
	</div>
	<style>
		.btn-login{
			width: 100%;
			padding: 10px 20px;
		}

		.tiep-tuc{
			display: inline-flex;
			padding: 10px 20px;
		}

		.tiep-tuc > p{
			font-size: 22px;
			font-weight: 200;
		}
	</style>
	<?php } ?>