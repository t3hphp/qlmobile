<?php 
@session_start();
/*print_r($_SESSION['cart']);
print_r($_SESSION['total']);*/ ?>

<div class="single-page">
	<div class="single-page-row" id="detail-21">
		<div class="col-md-6 single-top-left">	
			<div class="flexslider">
				<ul class="slides">
					<li data-thumb="images/<?php echo $dt->hinh ?>">
						<div class="thumb-image detail_images"> <img src="images/<?php echo $dt->hinh ?>" data-imagezoom="true" class="img-responsive1" alt=""> </div>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-md-6 single-top-right">
			<h3 class="item_name"><strong><?php echo $dt->ten_dien_thoai ?></strong></h3>
			<div class="single-rating">
				<div class="fb-like" data-href="<?php echo curPageURL() ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
			</div>
			<p class="single-price-text">
				<div id="tb-testimonial" class="testimonial testimonial-default">
					<div class="testimonial-section">
						<div class="single-price">
							<ul>
								<?php 
								if($dt->don_gia_khuyen_mai>1)
								{
									echo '<li class="dgkm">' . number_format($dt->don_gia_khuyen_mai) .' VNĐ' . '</li><br/>' . 
									'<li><del>' . number_format($dt->don_gia) . ' VNĐ' . '</del></li>' .
									'<li><span class="w3off"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> ' . ceil(($dt->don_gia - $dt->don_gia_khuyen_mai)/($dt->don_gia) * 100) .'% OFF</span></li><br/>' ;
								}
								else
								{
									echo '<li class="dgkm">' . number_format($dt->don_gia) .' VNĐ' . '</li>';
								}
								?>
								<hr/>
								<li><i class="fa fa-bell text-danger" aria-hidden="true"></i> Bảo hành chính hãng: <strong><?php echo $dt->bao_hanh ?></strong> tháng</li><br/>
								<li><i class="fa fa-paper-plane text-info" aria-hidden="true"></i> Free-shipping toàn quốc</li>
								<li><i class="fa fa-gift text-success" aria-hidden="true"></i> Nhiều quà tặng khuyến mãi bất ngờ khi nhận hàng</li>
							</ul>	
						</div> 
					</div>
				</div>   
			</p><br/>

			<!--gửi thông tin vào ajax_cart.js-->
			<input type="hidden" id="dongia<?php echo $dt->ma_dien_thoai ?>" value="<?php echo ($dt->don_gia_khuyen_mai>1)?$dt->don_gia_khuyen_mai:$dt->don_gia ?>" /> 
			<input type="hidden" id="sosanpham<?php echo $dt->ma_dien_thoai ?>" value="1" /> 
			<a href="javascript:void(0)" class="btn w3ls-cart" id="<?php echo $dt->ma_dien_thoai ?>"><i class="fa fa-cart-plus" aria-hidden="true"></i> Đặt hàng</a>

			<hr/>
			<p class="help-block"><strong>Lượt mua: </strong> <?php echo $dt->so_luot_mua ?> | <strong>Lượt xem: </strong> <?php echo $dt->so_luot_xem ?></p>
		</div>
		<div class="clearfix"> </div>
	</div>
</div> 

<div class="row" id="danh-gia-sp">
	<div class="col-md-10" >
		<!-- collapse-tabs -->
		<div class="collpse tabs">
			<h2 class="w3ls-title">Đánh giá sản phẩm</h2> 
			<div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default_1" data-toggle="tab">Mô tả</a>
						</li>
						<li>
							<a href="#tab_default_2" data-toggle="tab">Tóm tắt thông số</a>
						</li>
						<li>
							<a href="#tab_default_3" data-toggle="tab">Bình luận </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_1">
							<div class="shorten"><?php echo $dt->mo_ta ?></div>
						</div>
						<div class="tab-pane" id="tab_default_2">
							<?php echo $dt->tom_tat_thong_so ?>
						</div>
						<div class="tab-pane" id="tab_default_3">
							<div class="fb-comments" data-href="<?php echo curPageURL() ?>" data-numposts="5" width="100%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //collapse --> 
	</div>

	<div class="col-md-2" id="lien-he-quang-cao">
		<a href="huong-dan-mua-hang.php" target="_blank"><img src="images/hdmh.png" alt="Hướng dẫn mua hàng"></a>
		<hr/>
		<a href="lien-he.php" target="_blank"><img src="images/qc.png" alt="Quảng cáo"></a>
		<hr/>
		<a href="lien-he.php" target="_blank"><img src="images/qc.png" alt="Quảng cáo"></a>
	</div>
</div>


<!--Item text-->
<div class="container">
	<div class="row" id="slider-text">
		<div class="col-md-6" >
			<h2>Có thể bạn thích</h2>
		</div>
	</div>
</div>
<!-- Item-->
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="carousel carousel-showmanymoveone slide" id="itemslider">
				<div class="carousel-inner">
					<?php foreach($dt_rands as $dtr) {?>
					<div class="item active">
						<div class="col-xs-12 col-sm-6 col-md-2">
							<a href="dien-thoai.php?dt=<?php echo $dtr->ma_dien_thoai ?>"><img src="images/<?php echo $dtr->hinh ?>" class="img-responsive center-block"></a>
							<h4 class="text-center"><?php echo $dtr->ten_dien_thoai ?></h4>
							<h5 class="text-center">
								<?php echo number_format(isset($dtr->don_gia_khuyen_mai)?$dtr->don_gia_khuyen_mai:$dtr->don_gia) ?> VNĐ
							</h5>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Item slider end-->

<script type="text/javascript">
	$('.shorten').shorten({
		moreText: '<br/><button class="btn btn-sm btn-default center-block"> Xem thêm <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></button>',
		lessText: '<br/><button class="btn btn-sm btn-default center-block"> Thu gọn <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></button>',
		showChars: 1000
	});
</script>
