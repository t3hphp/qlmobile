<!--lọc giá, tên a-z-->
<div class="row">
	<div class="col-md-5 pull-right">
		<div class="row flex">
			<select class="form-control" name="sort" id="sort-gia">
				<option value="gia_giam">Sắp xếp theo giá GIẢM DẦN</option>
				<option value="gia_tang">Sắp xếp theo giá TĂNG DẦN</option>
			</select>
			<button class="btn btn-info" id="btn-sort-gia" style="margin-left:10px; margin-right:30px"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></button>
		</div>
	</div>
</div>