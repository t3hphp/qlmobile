<?php 
if(isset($_GET["nsx"]))
{
	$active = $_GET["nsx"];
}
else
{
	$active = "active";
}
?>

<div class="col-md-3 w3ls_dresses_grid_left">
	<div class="w3ls_dresses_grid_left_grid">
		<h3>Nhà sản xuất</h3>
		<div class="w3ls_dresses_grid_left_grid_sub">
			<div class="ecommerce_dres-type">
				<ul>
					<li><a href="loai-dien-thoai.php" class="<?php echo $active ?>">TẤT CẢ</a></li>
					<?php foreach($ten_nha_san_xuats as $nsx){ ?>
					<li><a href="loai-dien-thoai.php?nsx=<?php echo $nsx->ma_nha_san_xuat ?>" class="<?php echo (($nsx->ma_nha_san_xuat == $active) ? "active" : "") ?>"><?php echo $nsx->ten_nha_san_xuat ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="w3ls_dresses_grid_left_grid">
		<h3>Lọc theo giá thành</h3>
		<div class="w3ls_dresses_grid_left_grid_sub">
			<div class="ecommerce_color ecommerce_size">
				<ul>
					<li><p class="loc-gia-ne" id="loc-duoi-2">Dưới 2 triệu</p></li>
					<li><p class="loc-gia-ne" id="loc-2-5">2 - 5 triệu</p></li>
					<li><p class="loc-gia-ne" id="loc-5-10">5 - 10 triệu</p></li>
					<li><p class="loc-gia-ne" id="loc-tren-10">Trên 10 triệu</p></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	p.loc-gia-ne{
		cursor: pointer;
	}
</style>