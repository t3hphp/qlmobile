<?php 
if(!isset($dts))
{
	echo "<hr/><h3 class='text-muted'>Đang cập nhật!</h3><hr/>";
}
else if(count($dts)==0) //ko có sản phẩm thỏa điều kiện lọc
{
	echo "<br/><hr/><h3 class='text-muted'>Không có sản phẩm thỏa điều kiện lọc này!</h3><hr/>";
}
else
{
	?>
	<div class="col-md-9 col-sm-9 women-dresses">
		<?php if(!isset($co_sap_xep)){
			include("app/views/loai_dien_thoai/v_sap_xep_gia.php");
		}
		//ko bị lặp 2 lần chỗ này -> isset ?>
	</div>
	<div id="div_sap_xep">	
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-9 women-dresses">
					<!--sản phẩm-->
					<?php $count = 1;
					foreach($dts as $dt) { ?>
					<div class="col-md-4 women-grids wp1 animated wow slideInUp" data-wow-delay=".2s">
						<a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><div class="product-img">
							<img src="images/<?php echo $dt->hinh ?>" alt="" />
							<div class="p-mask">
								<button type="submit" class="w3ls-cart-a pw3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Xem chi tiết</button>
							</div>
						</div></a>
						<h4><a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><?php echo $dt->ten_dien_thoai ?></a></h4>
						<h5><?php 
							if($dt->don_gia_khuyen_mai>1)
							{
								echo '<del>' . number_format($dt->don_gia) .' VNĐ' . '</del>' . '<div class="dgkm">' . number_format($dt->don_gia_khuyen_mai) . ' VNĐ' . '</div>';
							}
							else
							{
								echo number_format($dt->don_gia) .' VNĐ';
							}
							?>
						</h5>
					</div>

					<?php 
					if($count % 3 == 0){	echo '<div class="clearfix"></div>';	}
					$count++;
				} ?>
				<br/>
				<div class="clearfix"></div>
				<ul class="pagination text-center center-block">
					<?php echo isset($list)?$list:"" ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>