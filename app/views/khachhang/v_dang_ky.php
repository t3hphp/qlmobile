<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" method="post">
			<h2>Hãy đăng kí tài khoản <strong>QLMobile</strong><br/><small>Vì nó miễn phí và mãi mãi là thế.</small></h2>
			<br/><h4 class="text-danger">Thông tin sẽ không được thay đổi lại (do lười code), vui lòng nhập chính xác xíu. Cảm ơn!</h4>
			<hr class="colorgraph">	
			<div class="form-group">
				<input type="text" name="ho_ten" id="ho_ten" class="form-control input-lg kiemtra" placeholder="Họ và tên" tabindex="1" data_error="Nhập họ và tên" value="<?php echo $ho_ten ?>">
			</div>
			<div class="form-group">
				<p class="help-block" id="ajax-tk-trung"></p>
				<input type="text" name="tai_khoan" id="tai_khoan" class="form-control input-lg kiemtra" maxlength="12" placeholder="Tài khoản" tabindex="2" data_error="Nhập tài khoản" value="<?php echo $tai_khoan ?>">
			</div>
			<div class="form-group">
				<input type="password" name="mat_khau" id="mat_khau" class="form-control input-lg kiemtra" placeholder="Mật khẩu" tabindex="3" data_error="Nhập mật khẩu">
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="email" name="email" id="email" class="form-control input-lg kiemtra" placeholder="Email" tabindex="4" data_error="Nhập email" value="<?php echo $email ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="dien_thoai" id="dien_thoai" class="form-control input-lg kiemtra" placeholder="Điện thoại" tabindex="5" data_error="Nhập số điện thoại" value="<?php echo $dien_thoai ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="text" name="dia_chi" id="dia_chi" class="form-control input-lg kiemtra" placeholder="Địa chỉ" tabindex="6" data_error="Nhập địa chỉ" value="<?php echo $dia_chi ?>">
			</div>

			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7"> Đồng ý</button>
						<input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
					</span>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-9">
					Khi click vào <strong class="label label-primary">Đăng ký</strong>, bạn đã đồng ý các <a href="#" data-toggle="modal" data-target="#t_and_c_m">Chính sách và Điều khoản</a> của hệ thống.
				</div>
			</div>
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<input type="submit" name="btn_dang_ky" value="Đăng ký" onclick="return Kiemtradulieu()" class="btn btn-primary btn-block btn-lg" tabindex="8"></div>
					<div class="col-xs-12 col-md-6"><a href="dang-nhap.php" class="btn btn-success btn-block btn-lg">Đăng nhập</a></div>
				</div>
			</form>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Chính sách và Điều khoản</h4>
				</div>
				<div class="modal-body">
					<p class="text-center">Đang cập nhật...</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Đồng ý</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
</div><!-- /.modal -->