<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" method="post">
			<fieldset>
				<h2>Đăng nhập vào <strong>QLMobile</strong></h2>
				<hr class="colorgraph">
				<div class="form-group">
					<input type="text" name="tai_khoan" id="tai_khoan" class="form-control input-lg" placeholder="Tên tài khoản" value="<?php echo (isset($tai_khoan))?$tai_khoan:"" ?>">
				</div>
				<div class="form-group">
					<input type="password" name="mat_khau" id="mat_khau" class="form-control input-lg" placeholder="Mật khẩu">
				</div>
				<?php 
				if(isset($err))
				{
					echo '<div class="alert alert-danger" role="alert"><strong>Oh nooooo! </strong>' . $err . '</div>';
				}
				?>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_dang_nhap" value="Đăng nhập">
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<a href="dang-ky.php" class="btn btn-lg btn-primary btn-block">Đăng ký</a>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
