<div class="top-products">
	<div class="container">
		<h3>Sản phẩm TOP</h3>
		<div class="sap_tabs">			
			<div id="horizontalTab">
				<ul class="resp-tabs-list">
					<li class="resp-tab-item"><span>Apple</span></li>
					<li class="resp-tab-item"><span>Sản phẩm bán chạy</span></li>
					<li class="resp-tab-item"><span>Samsung</span></li>
				</ul>
				<div class="clearfix"> </div>	
				<div class="resp-tabs-container">
					<div class="tab-1 resp-tab-content">
						<?php 
						$count=1;
						foreach($apples as $dt) { ?>
						<div class="col-md-3 top-product-grids tp1 animated wow slideInUp" data-wow-delay=".5s">
							<a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><div class="product-img">
								<img src="images/<?php echo $dt->hinh ?>" alt="" />
								<div class="p-mask">
									<button type="submit" class="w3ls-cart-a pw3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Xem chi tiết</button>
								</div>
							</div></a>
							<h4><a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><?php echo $dt->ten_dien_thoai ?></a></h4>
							<h5><?php 
								if($dt->don_gia_khuyen_mai>1)
								{
									echo '<del>' . number_format($dt->don_gia) .' VNĐ' . '</del>' . '<div class="dgkm">' . number_format($dt->don_gia_khuyen_mai) . ' VNĐ' . '</div>';
								}
								else
								{
									echo number_format($dt->don_gia) .' VNĐ';
								}
								?>
							</h5>
						</div>
						<?php 
						if($count % 4 == 0)
						{
							echo '<div class="clearfix"></div>';
						}
						$count++;
					} ?>
					<br/>
					<div class="row">
						<div class="text-right">
							<a href="loai-dien-thoai.php?nsx=1" class="btn btn-lg btn-default">Xem các sản phẩm Apple khác <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>

				<div class="tab-1 resp-tab-content">
					<?php 
					$count=1;
					foreach($banchays as $dt) { ?>
					<div class="col-md-3 top-product-grids tp1 animated wow slideInUp" data-wow-delay=".5s">
						<a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><div class="product-img">
							<img src="images/<?php echo $dt->hinh ?>" alt="" />
							<div class="p-mask">
								<button type="submit" class="w3ls-cart-a pw3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Xem chi tiết</button>
							</div>
						</div></a>
						<h4><a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><?php echo $dt->ten_dien_thoai ?></a></h4>
						<h5><?php 
							if($dt->don_gia_khuyen_mai>1)
							{
								echo '<del>' . number_format($dt->don_gia) .' VNĐ' . '</del>' . '<div class="dgkm">' . number_format($dt->don_gia_khuyen_mai) . ' VNĐ' . '</div>';
							}
							else
							{
								echo number_format($dt->don_gia) .' VNĐ';
							}
							?>
						</h5>
					</div>
					<?php 
					if($count % 4 == 0)
					{
						echo '<div class="clearfix"></div>';
					}
					$count++;
				} ?>
				<br/>
				<div class="row">
					<div class="text-right">
						<a href="loai-dien-thoai.php" class="btn btn-lg btn-default">Xem các sản phẩm Bán chạy khác <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>

			<div class="tab-1 resp-tab-content">
				<?php 
				$count=1;
				foreach($samsungs as $dt) { ?>
				<div class="col-md-3 top-product-grids tp1 animated wow slideInUp" data-wow-delay=".5s">
					<a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><div class="product-img">
						<img src="images/<?php echo $dt->hinh ?>" alt="" />
						<div class="p-mask">
							<button type="submit" class="w3ls-cart-a pw3ls-cart"><i class="fa fa-cart-plus" aria-hidden="true"></i> Xem chi tiết</button>
						</div>
					</div></a>
					<h4><a href="dien-thoai.php?dt=<?php echo $dt->ma_dien_thoai ?>"><?php echo $dt->ten_dien_thoai ?></a></h4>
					<h5><?php 
						if($dt->don_gia_khuyen_mai>1)
						{
							echo '<del>' . number_format($dt->don_gia) .' VNĐ' . '</del>' . '<div class="dgkm">' . number_format($dt->don_gia_khuyen_mai) . ' VNĐ' . '</div>';
						}
						else
						{
							echo number_format($dt->don_gia) .' VNĐ';
						}
						?>
					</h5>
				</div>
				<?php 
				if($count % 4 == 0)
				{
					echo '<div class="clearfix"></div>';
				}
				$count++;
			} ?>
			<br/>
			<div class="row">
				<div class="text-right">
					<a href="loai-dien-thoai.php?nsx=2" class="btn btn-lg btn-default">Xem các sản phẩm Samsung khác <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
</div>
</div>


<script src="public/layout/js/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true   // 100% fit in a container
			});
	});		
</script>