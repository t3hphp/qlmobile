<!-- newsletter -->
<div class="newsletter hidden-print">
	<div class="container">
		<div class="col-md-6 w3agile_newsletter_left">
			<h3>Đăng kí nhận tin</h3>
			<p>Nhận tin về sản phẩm mới nhất, chương trình khuyến mãi nhanh nhất</p>
		</div>
		<div class="col-md-6 w3agile_newsletter_right">
			<form action="#" method="post">
				<input type="email" name="email" placeholder="Email" required="required">
				<input type="submit" value="Subscribe" name="dang_ky_nhan_tin" id="sub"/>
				<p class="help-block" id="sub-ok">Chúng tôi đã ghi nhận thông tin! Cảm ơn bạn đã ghé thăm QLMobile</p>
			</form>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>

<script type="text/javascript">
	$(function()
	{
		$("#sub-ok").hide();
		$("#sub").click(function()
		{
			$("#sub-ok").fadeIn('slow');
		})
	});
</script>