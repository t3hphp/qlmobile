<?php 
include_once("config/database.php");
class M_khach_hang extends database
{
	public function Doc_ds_khach_hang($tai_khoan, $mat_khau)
	{
		$sql = "select * from user where tai_khoan = ? && mat_khau = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($tai_khoan, $mat_khau));
	}

	//ma_khach_hang, ho_ten, tai_khoan, mat_khau, email, dia_chi, dien_thoai, role
	public function Them_khach_hang($ho_ten, $tai_khoan, $mat_khau, $email, $dia_chi, $dien_thoai)
	{
		$sql = "insert into user values (?, ?, ?, ?, ?, ?, ?, ?)";
		$this->setQuery($sql);
		$param = array(NULL, $ho_ten, $tai_khoan, $mat_khau, $email, $dia_chi, $dien_thoai, "customer");
		return $this->execute($param);
	}

	//kiểm tra thêm thành công hay ko
	public function getRowCount() 
	{
		$sql = "select count(*) from user";
		$this->setQuery($sql);
		return $this->loadRecord();
	}

	public function Doc_tai_khoan_theo_ten_tai_khoan($tai_khoan)
	{
		$sql = "select * from user where tai_khoan = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($tai_khoan));
	}
}
 ?>
