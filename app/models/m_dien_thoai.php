<?php 
include_once("config/database.php");
class M_dien_thoai extends database
{
	public function Doc_dien_thoai($ma_dien_thoai)
	{
		$sql = "select * from dien_thoai where ma_dien_thoai = ?";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_dien_thoai));
	}

	public function Doc_dt_random($ma_dien_thoai)
	{
		$sql = "SELECT * FROM dien_thoai where ma_dien_thoai != ? order by RAND() LIMIT 6";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_dien_thoai));
	}

	public function Lay_dien_thoai_trong_gio_hang($chuoi)
	{
		$sql = "select * from dien_thoai where ma_dien_thoai in ($chuoi)";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	//Đếm lượt xem
	public function Dem_luot_xem($ma_dien_thoai)
	{
		$sql = "update dien_thoai set so_luot_xem = so_luot_xem + 1 where ma_dien_thoai = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_dien_thoai));
	}
}
 ?>