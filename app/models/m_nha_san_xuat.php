<?php 
include_once("config/database.php");
class M_nha_san_xuat extends database
{
	public function Doc_ten_nha_san_xuat()
	{
		$sql = "select * from nha_san_xuat";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_ten_nha_san_xuat_theo_ma($ma_nha_san_xuat)
	{
		$sql = "select ten_nha_san_xuat from nha_san_xuat where ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_nha_san_xuat));
	}

	//Menu
	public function Doc_ten_nha_san_xuat_07()
	{
		$sql = "select * from nha_san_xuat limit 0,6";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Doc_ten_nha_san_xuat_7end()
	{
		$sql = "select * from nha_san_xuat limit 6,999";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}

 ?>