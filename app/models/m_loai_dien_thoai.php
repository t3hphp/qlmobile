<?php 
include_once("config/database.php");
class M_loai_dien_thoai extends database
{
	public function Doc_dien_thoai_cua_nsx($ma_nha_san_xuat, $vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai where ma_nha_san_xuat=? order by RAND()";
		if($vt>=0 && $limit>0)
		{
			$sql .=" limit $vt,$limit";
		}
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Doc_dien_thoai_moi_nhat($vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai order by ngay_cap_nhat desc";
		if($vt>=0 && $limit>0)
		{
			$sql .=" limit $vt,$limit";
		}
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Dem_so_san_pham_nsx($ma_nha_san_xuat)
	{
		$sql = "select count(*) from dien_thoai where ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadRecord(array($ma_nha_san_xuat));
	}

	//Tìm kiếm
	public function Tim_kiem($ten_dien_thoai)
	{
		$sql = "select * from dien_thoai where ten_dien_thoai like '%" . $ten_dien_thoai . "%'";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	//Sắp xếp theo giá
	public function Sap_xep_gia_giam_theo_ma_nha_sx($ma_nha_san_xuat, $vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai where ma_nha_san_xuat = ? ORDER BY don_gia desc";
		if($vt >= 0 && $limit > 0)
		{
			$sql .= " limit $vt, $limit";
		}
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Sap_xep_gia_tang_theo_ma_nha_sx($ma_nha_san_xuat, $vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai where ma_nha_san_xuat = ? ORDER BY don_gia asc";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Sap_xep_gia_giam_tat_ca($vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai ORDER BY don_gia desc";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Sap_xep_gia_tang_tat_ca($vt = -1, $limit = -1)
	{
		$sql = "select * from dien_thoai ORDER BY don_gia asc";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	//Lọc giá
	//Có nsx
	public function Loc_gia_duoi_2_co_nsx($ma_nha_san_xuat)
	{
		$sql = "select * from dien_thoai where don_gia < 2000000 and ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Loc_gia_2_5_co_nsx($ma_nha_san_xuat)
	{
		$sql = "select * from dien_thoai where don_gia < 5000000 and don_gia >= 2000000 and ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Loc_gia_5_10_co_nsx($ma_nha_san_xuat)
	{
		$sql = "select * from dien_thoai where don_gia < 10000000 and don_gia >= 5000000 and ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	public function Loc_gia_tren_10_co_nsx($ma_nha_san_xuat)
	{
		$sql = "select * from dien_thoai where don_gia > 10000000 and ma_nha_san_xuat = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_nha_san_xuat));
	}

	//Tất cả
	public function Loc_gia_duoi_2()
	{
		$sql = "select * from dien_thoai where don_gia < 2000000";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Loc_gia_2_5()
	{
		$sql = "select * from dien_thoai where don_gia < 5000000 and don_gia >= 2000000";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Loc_gia_5_10()
	{
		$sql = "select * from dien_thoai where don_gia < 10000000 and don_gia >= 5000000";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}

	public function Loc_gia_tren_10()
	{
		$sql = "select * from dien_thoai where don_gia > 10000000";
		$this->setQuery($sql);
		return $this->loadAllRows();
	}
}
 ?>
