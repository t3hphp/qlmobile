<?php 
include_once("config/database.php");
class M_don_hang extends database
{
	public function Doc_don_hang_theo_kh($ma_khach_hang)
	{
		$sql = "select * from hoa_don where ma_khach_hang = ? order by ngay_dat_hang desc, ma_hoa_don desc limit 0,5";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_khach_hang));
	}

	public function Doc_chi_tiet_hoa_don($ma_khach_hang)
	{

		$sql = "select hd.*, count(hd.ma_hoa_don) as count, GROUP_CONCAT(dt.ten_dien_thoai) as dien_thoai, GROUP_CONCAT(cthd.so_luong) as so_luong from hoa_don hd, chi_tiet_hoa_don cthd, dien_thoai dt where hd.ma_hoa_don = cthd.ma_hoa_don and cthd.ma_dien_thoai = dt.ma_dien_thoai and hd.ma_khach_hang=? group by hd.ma_hoa_don order by ngay_dat_hang desc, ma_hoa_don desc limit 0,5";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_khach_hang));
	}
}
 ?>