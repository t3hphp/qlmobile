<?php 
include_once("config/database.php");
class M_hoa_don extends database
{
	//ma_hoa_don, ma_khach_hang, ngay_dat_hang, trang_thai, tong_tien
	public function Them_hoa_don($ma_khach_hang, $ngay_dat_hang, $tong_tien)
	{
		$sql = "insert into hoa_don values (?,?,?,?,?)";
		$this->setQuery($sql);
		$result = $this->execute(array(NULL, $ma_khach_hang, $ngay_dat_hang, 0, $tong_tien));
		if($result)
		{
			return $this->getLastId();
		}
		else
			return false;
	}

	//ma_hoa_don, ma_dien_thoai, so_luong, don_gia
	public function Them_chi_tiet_hoa_don($ma_hoa_don, $ma_dien_thoai, $so_luong, $don_gia)
	{
		$sql = "insert into chi_tiet_hoa_don values(?,?,?,?)";
		$this->setQuery($sql);
		return $this->execute(array($ma_hoa_don, $ma_dien_thoai, $so_luong, $don_gia));
	}

	public function Cap_nhat_don_gia($ma_hoa_don)
	{
		$sql = "update chi_tiet_hoa_don
		set don_gia = (select don_gia from dien_thoai where chi_tiet_hoa_don.ma_dien_thoai = dien_thoai.ma_dien_thoai)
		where ma_hoa_don = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_hoa_don));
	}

	public function Cap_nhat_tong_tien($ma_hoa_don)
	{
		$sql = "update hoa_don
		set tong_tien = (select sum(so_luong * don_gia) from chi_tiet_hoa_don where chi_tiet_hoa_don.ma_hoa_don = hoa_don.ma_hoa_don)
		where ma_hoa_don = ?";
		$this->setQuery($sql);
		return $this->execute(array($ma_hoa_don));
	}

	/*Lấy số hóa đơn mới nhất của khách hàng đó để in ra
	Tránh trường hợp 2 người đặt cùng lúc sẽ bị nhầm hóa đơn*/
	public function Lay_hoa_don_moi_nhat($ma_khach_hang)
	{
		$sql = "select * from hoa_don where ma_khach_hang = ? order by ma_hoa_don desc limit 0,1";
		$this->setQuery($sql);
		return $this->loadRow(array($ma_khach_hang));
	}

	public function Lay_cthd_moi_nhat($ma_hoa_don)
	{
		$sql = "select *,dien_thoai.ten_dien_thoai from chi_tiet_hoa_don, dien_thoai where dien_thoai.ma_dien_thoai = chi_tiet_hoa_don.ma_dien_thoai and ma_hoa_don = ?";
		$this->setQuery($sql);
		return $this->loadAllRows(array($ma_hoa_don));
	}

	public function Cap_nhat_so_luong_san_pham($so_luong, $ma_dien_thoai)
	{
		$sql = "update dien_thoai set so_luot_mua = so_luot_mua + ? where ma_dien_thoai = ?";
		$this->setQuery($sql);
		return $this->execute(array($so_luong, $ma_dien_thoai));
	}
}

 ?>