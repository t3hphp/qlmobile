<?php 
@session_start();
class C_gio_hang
{
	public function Them_gio_hang($ma_dien_thoai, $so_san_pham, $don_gia)
	{
		if($so_san_pham > 0)
		{
			if(isset($_SESSION['cart'][$ma_dien_thoai])) {
				$_SESSION['cart']['da_co_sp_nay'] = 1;
				$_SESSION['total'] -= @$_SESSION['cart'][$ma_dien_thoai] * $don_gia;
				$_SESSION['amount'] -= @$_SESSION['cart'][$ma_dien_thoai];      
			}

			//1 điện thoại có số lượng lúc chọn là 1, không cho thêm sản phẩm trùng lúc chọn hàng
			//(vào giỏ hàng để cập nhật số lượng)

			$_SESSION['cart'][$ma_dien_thoai] = $so_san_pham;
			//Array(1 => 2) :: 1 là mã, 2 là số lượng

			if(isset($_SESSION['total']))
			{
				$_SESSION['total'] += $so_san_pham * $don_gia;
				$_SESSION['amount'] += $so_san_pham;
			}
			else
			{
				$_SESSION['total'] = $so_san_pham * $don_gia;
				$_SESSION['amount'] = $so_san_pham;
			}
		}
	}

	public function cart()
	{
		return isset($_SESSION['cart'])?$_SESSION['cart']:false;
	}

	public function total()
	{
		return isset($_SESSION['total'])?$_SESSION['total']:0;
	}

	public function amount()
	{
		return isset($_SESSION['amount'])?$_SESSION['amount']:0;
	}

	public function Cap_nhat_gio_hang($ma_dien_thoai, $so_san_pham, $don_gia)
	{
		if(!is_numeric($so_san_pham))
			return false;

		$so_san_pham = (int)$so_san_pham;
		if($so_san_pham > 0)
		{
			$_SESSION['total'] -= @$_SESSION['cart'][$ma_dien_thoai] * $don_gia;
			$_SESSION['total'] += $so_san_pham * $don_gia;

			$_SESSION['amount'] -= @$_SESSION['cart'][$ma_dien_thoai];
			$_SESSION['amount'] += $so_san_pham;

			$_SESSION['cart'][$ma_dien_thoai] = $so_san_pham;

			return false;
		}
	}

	public function Xoa_mat_hang($ma_dien_thoai, $don_gia)
	{
		$gio_hang = $this->cart();
		$gio_hang_moi = array();
		foreach ($gio_hang as $key => $value)
		{
			if($key != $ma_dien_thoai)
			{
				$gio_hang_moi[$key] = $value;
			}
			else
			{
				$_SESSION['total'] -= $gio_hang[$ma_dien_thoai] * $don_gia;
				$_SESSION['amount'] -= $gio_hang[$ma_dien_thoai];
			}
		}

		if(!empty($gio_hang_moi))
		{
			$_SESSION['cart'] = $gio_hang_moi;
		}
		else
		{
			$this->Xoa_gio_hang();
		}
		
	}

	public function Xoa_gio_hang()
	{
		unset($_SESSION['cart']);
		unset($_SESSION['total']);
		unset($_SESSION['amount']);
	}

	public function Shopping_cart_icon()
	{
		//Model
		$gio_hang = $this->cart();
		$array_ma_dt = "";
		if($gio_hang) //Nếu có giỏ hàng
		{
			foreach($gio_hang as $key=>$value)
			{
				$array_ma_dt[] = $key; //lấy mã điện thoại trong giỏ hàng
			}
			$array_ma_dt = implode(',', $array_ma_dt);

			//Khai báo phương thức lấy điện thoại trong giỏ trong model
			include_once("app/models/m_dien_thoai.php");
			$m_dien_thoai = new M_dien_thoai();
			$ds_dien_thoai = $m_dien_thoai->Lay_dien_thoai_trong_gio_hang($array_ma_dt);
			//print_r($ds_dien_thoai);

			//Đưa số lượng vào ds điện thoại
			$ds_dien_thoai_gio_hang = array();
			foreach($ds_dien_thoai as $item)
			{
				$item->sl = $gio_hang[$item->ma_dien_thoai]; //Array $gio_hang[3] = 1 --> 3- id, 1 - số lượng
				$ds_dien_thoai_gio_hang[] = $item; //nguồn cho view
			}
		}
		//Views
		include("app/views/gio_hang/v_shopping_cart.php");
	}

	public function Hien_thi_gio_hang_1()
	{
		//Model
		//lấy $_SESSION['cart']
		$gio_hang = $this->cart();
		$array_ma_dt = "";
		if($gio_hang) //Nếu có giỏ hàng
		{
			foreach($gio_hang as $key=>$value)
			{
				$array_ma_dt[] = $key; //lấy mã điện thoại trong giỏ hàng
			}
			$array_ma_dt = implode(',', $array_ma_dt);

			//Khai báo phương thức lấy điện thoại trong giỏ trong model
			include_once("app/models/m_dien_thoai.php");
			$m_dien_thoai = new M_dien_thoai();
			$ds_dien_thoai = $m_dien_thoai->Lay_dien_thoai_trong_gio_hang($array_ma_dt);
			//print_r($ds_dien_thoai);

			//Đưa số lượng vào ds điện thoại
			$ds_dien_thoai_gio_hang = array();
			foreach($ds_dien_thoai as $item)
			{
				$item->sl = $gio_hang[$item->ma_dien_thoai]; //Array $gio_hang[3] = 1 --> 3- id, 1 - số lượng
				$ds_dien_thoai_gio_hang[] = $item; //nguồn cho view
			}
		}

		//Gọi hàm Cập nhật giỏ hàng -> gio-hang.php (tự động refresh (để trước Trang_gio_hang))

		//View
		include("app/views/gio_hang/v_gio_hang_1.php");
	}

	public function Hien_thi_login_2()
	{
		//Model
		if(isset($_SESSION["tai_khoan"]))
		{
			$tai_khoan_kh = $_SESSION["tai_khoan"];
			include("app/models/m_khach_hang.php");
			$m_khach_hang = new M_khach_hang();
			$kh = $m_khach_hang->Doc_tai_khoan_theo_ten_tai_khoan($tai_khoan_kh);
			$ma_khach_hang = $kh->ma_khach_hang;
		}
		//In hóa đơn
		if(isset($_POST["in-hoa-don"]))
		{
			$ngay_dat_hang = date("Y-m-d");
			$tong_tien = 0; //cập nhật sau
			include_once("app/models/m_hoa_don.php");
			$m_hoa_don = new M_hoa_don();
			$hoa_don = $m_hoa_don->Them_hoa_don($ma_khach_hang, $ngay_dat_hang, $tong_tien);

			if($hoa_don > 0)
			{
				$gio_hang = $this->cart(); //lấy thông tin giỏ hàng
				foreach ($gio_hang as $key => $value) {
					$m_hoa_don->Them_chi_tiet_hoa_don($hoa_don, $key, $value, 0); //đơn giá cập nhật sau
					$m_hoa_don->Cap_nhat_so_luong_san_pham($value, $key);
				}
				$m_hoa_don->Cap_nhat_don_gia($hoa_don);
				$m_hoa_don->Cap_nhat_tong_tien($hoa_don);

				//Xóa session giỏ hàng
				$this->Xoa_gio_hang();
				echo '<script>swal("GREATTT!", "Đơn hàng đang được xử lý...", "success")</script>';
				header("refresh:2;url=don-hang.php");
			}
		}
		//View
		include("app/views/gio_hang/v_login_2.php");
	}

	public function Hien_thi_tom_tat_don_hang_3()
	{
		//Model
		$tai_khoan_kh = $_SESSION["tai_khoan"];
		include("app/models/m_khach_hang.php");
		$m_khach_hang = new M_khach_hang();
		$kh = $m_khach_hang->Doc_tai_khoan_theo_ten_tai_khoan($tai_khoan_kh);
		$ma_khach_hang = $kh->ma_khach_hang;

		include("app/models/m_hoa_don.php");
		$m_hoa_don = new M_hoa_don();
		$hoa_don_moi_nhat = $m_hoa_don->Lay_hoa_don_moi_nhat($ma_khach_hang);
		$cthd_moi_nhats = $m_hoa_don->Lay_cthd_moi_nhat($hoa_don_moi_nhat->ma_hoa_don);

		//View
		include("app/views/gio_hang/v_tom_tat_don_hang_3.php");
	}

	//=============================================
	//Function hiển thị trang gio-hang.php
	public function Trang_gio_hang()
	{
		//Views
		$title = "Giỏ hàng :: QLMobile";
		$view = "app/views/gio_hang/v_layout_gio_hang.php";
		include_once("public/include/layout_pages.php");
	}

	public function Trang_xac_thuc()
	{
		//Views
		$title = "Xác thực :: QLMobile";
		$view = "app/views/gio_hang/v_layout_login.php";
		include_once("public/include/layout_pages.php");
	}

	public function Trang_don_hang()
	{
		//Views
		$title = "Đơn hàng :: QLMobile";
		$view = "app/views/gio_hang/v_layout_don_hang.php";
		include_once("public/include/layout_pages.php");
	}

}

?>