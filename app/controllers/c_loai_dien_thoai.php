<?php
session_start();
include_once("app/models/m_nha_san_xuat.php");
include_once("app/models/m_loai_dien_thoai.php");
class C_loai_dien_thoai
{
	var $ten_nha_san_xuat_theo_ma = ""; //title

	public function Hien_thi_loai_dien_thoai()
	{
		//Models
		//Hiển thị nhà sản xuất
		$ten_nha_san_xuats = $this->Hien_thi_nha_san_xuat();

		//Hiển thị danh sách điện thoại
		if(isset($_GET["nsx"]))
		{
			$ma_nha_san_xuat = $_GET["nsx"];
			$this->ten_nha_san_xuat_theo_ma = $this->Hien_thi_ten_nha_san_xuat_theo_ma($ma_nha_san_xuat);

			$m_loai_dien_thoai = new M_loai_dien_thoai();

			$so_sp = $m_loai_dien_thoai->Dem_so_san_pham_nsx($ma_nha_san_xuat);
			if($so_sp != 0)
			{
				$dts = $m_loai_dien_thoai->Doc_dien_thoai_cua_nsx($ma_nha_san_xuat);
				//Phân trang
				include("config/Pager.php");
				$p = new pager();
				$limit = 9;
				$count = count($dts);
				$vt = $p->findStart($limit);
				$pages = $p->findPages($count, $limit);
				$curpage = $_GET["page"];
				$list = $p->pageList($curpage, $pages);
				$dts = $m_loai_dien_thoai->Doc_dien_thoai_cua_nsx($ma_nha_san_xuat, $vt, $limit);
			}
		}
		else //Trang tất cả
		{
			//Model
			$m_loai_dien_thoai = new M_loai_dien_thoai();
			$dts = $m_loai_dien_thoai->Doc_dien_thoai_moi_nhat();

			//Phân trang
			include("config/Pager.php");
			$p = new pager();
			$limit = 9;
			$count = count($dts);
			$vt = $p->findStart($limit);
			$pages = $p->findPages($count, $limit);
			$curpage = $_GET["page"];
			$list = $p->pageList($curpage, $pages);
			$dts = $m_loai_dien_thoai->Doc_dien_thoai_moi_nhat($vt, $limit);
		}		
		
		//Views
		$title = "Mua điện thoại $this->ten_nha_san_xuat_theo_ma giá tốt, chính hãng :: QLMobile";
		$view="app/views/loai_dien_thoai/v_loai_dien_thoai.php";
		include("public/include/layout_pages.php");
	}
	
	public function Hien_thi_nha_san_xuat()
	{
		//Model
		$m_nha_san_xuat = new M_nha_san_xuat();
		$ten_nha_san_xuats = $m_nha_san_xuat->Doc_ten_nha_san_xuat();
		return $ten_nha_san_xuats;
	}

	public function Hien_thi_ten_nha_san_xuat_theo_ma($ma_nha_san_xuat)
	{
		$m_nha_san_xuat = new M_nha_san_xuat();
		$ten_nha_san_xuat = $m_nha_san_xuat->Doc_ten_nha_san_xuat_theo_ma($ma_nha_san_xuat);
		return $ten_nha_san_xuat;
	}

	public function Xu_ly_sap_xep($type_sort, $nsx)
	{
		$type_sort = $_POST["type"];
		$nsx = $_POST["nsx"];
		
		$m_loai_dien_thoai = new M_loai_dien_thoai();
		if($nsx != 0) //có nsx
		{
			if($type_sort == 'gia_giam')
			{
				$dts = $m_loai_dien_thoai->Sap_xep_gia_giam_theo_ma_nha_sx($nsx);
			}
			else
			{
				$dts = $m_loai_dien_thoai->Sap_xep_gia_tang_theo_ma_nha_sx($nsx);
			}
		}
		else
		{
			if($type_sort == 'gia_giam')
			{
				$dts = $m_loai_dien_thoai->Sap_xep_gia_giam_tat_ca();
			}
			else
			{
				$dts = $m_loai_dien_thoai->Sap_xep_gia_tang_tat_ca();
			}
		}

		//Views
		$co_sap_xep = true;
		include("app/views/loai_dien_thoai/v_ds_dien_thoai.php");
	}

	public function Xu_ly_loc_gia($type_gia, $nsx)
	{
		$type_gia = $_POST["type"];
		$nsx = $_POST["nsx"];
		
		$m_loai_dien_thoai = new M_loai_dien_thoai();
		if($nsx != 0) //có nsx
		{
			//dưới 2 triệu
			if($type_gia == 'loc-duoi-2')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_duoi_2_co_nsx($nsx);
			}
			else if($type_gia == 'loc-2-5')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_2_5_co_nsx($nsx);
			}
			else if($type_gia == 'loc-5-10')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_5_10_co_nsx($nsx);
			}
			else
			{
				$dts = $m_loai_dien_thoai->Loc_gia_tren_10_co_nsx($nsx);
			}
		}
		else //tất cả
		{
			if($type_gia == 'loc-duoi-2')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_duoi_2();
			}
			else if($type_gia == 'loc-2-5')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_2_5();
			}
			else if($type_gia == 'loc-5-10')
			{
				$dts = $m_loai_dien_thoai->Loc_gia_5_10();
			}
			else
			{
				$dts = $m_loai_dien_thoai->Loc_gia_tren_10();
			}
		}

		//Views
		$co_sap_xep = true;
		include("app/views/loai_dien_thoai/v_ds_dien_thoai.php");
	}
}