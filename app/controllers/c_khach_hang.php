<?php 
@session_start();
include("app/models/m_khach_hang.php");
class C_khach_hang
{
	public function Dang_nhap()
	{
		if(isset($_POST["btn_dang_nhap"]))
		{
			$tai_khoan = $_POST["tai_khoan"];
			$mat_khau = md5($_POST["mat_khau"]);
			$m_khach_hang = new M_khach_hang();
			$kq = $m_khach_hang->Doc_ds_khach_hang($tai_khoan, $mat_khau);
			if($kq)
			{
				$_SESSION["ma_khach_hang"] = $kq->ma_khach_hang;
				$_SESSION["tai_khoan"] = $kq->tai_khoan;
				$_SESSION["khach_hang"] = $kq->ho_ten;
				if(isset($_SESSION['cart']))
				{
					header('location:xac-thuc.php');
				}
				else
				{
					header('location: ' . $_SERVER['HTTP_REFERER']);
				}
			}
			else
			{
				$err = "Bạn đã nhập sai tài khoản hoặc mật khẩu! Nếu chưa có tài khoản, hãy đăng ký mới.";
			}
		}
		//Views
		$title = "Đăng nhập QLMobile";
		$view = "app/views/khachhang/v_dang_nhap.php";
		include("public/include/layout_pages.php");
	}

	public function Dang_ky()
	{
		$m_khach_hang = new M_khach_hang();
		$lastID = $m_khach_hang->getRowCount();
		//ma_khach_hang, ho_ten, tai_khoan, mat_khau, email, dia_chi, dien_thoai, role
		$ho_ten=$tai_khoan=$mat_khau=$email=$dia_chi=$dien_thoai="";
		if(isset($_POST["btn_dang_ky"]))
		{
			$ho_ten = $_POST["ho_ten"];
			$tai_khoan = $_POST["tai_khoan"];
			$mat_khau = md5($_POST["mat_khau"]);
			$email = $_POST["email"];
			$dia_chi = $_POST["dia_chi"];
			$dien_thoai = $_POST["dien_thoai"];
			//echo $ho_ten, $tai_khoan, $mat_khau, $email, $dia_chi, $dien_thoai;
			$m_khach_hang->Them_khach_hang($ho_ten, $tai_khoan, $mat_khau, $email, $dia_chi, $dien_thoai);
			$newLastID = $m_khach_hang->getRowCount();
			if($lastID != $newLastID)
			{
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { swal("WOW!","Bạn đã đăng ký tài khoản thành công! Hãy đăng nhập vào hệ thống","success");}, 1000);';
				echo 'setTimeout(function () { location.href="dang-nhap.php"} , 3000);';
				echo '</script>';
			}
			else
			{
				echo '<script type="text/javascript">';
				echo 'setTimeout(function () { swal("Error","Tài khoản bạn chọn đã bị trùng","error");}, 1000);';
				echo '</script>';
			}
		}
		//Views
		$title = "Đăng ký tài khoản tại QLMobile";
		$view = "app/views/khachhang/v_dang_ky.php";
		include("public/include/layout_pages.php");
	}

	public function Hien_thi_thong_tin_tai_khoan()
	{
		//Model
		$m_khach_hang = new M_khach_hang();
		$tk = $m_khach_hang->Doc_tai_khoan_theo_ten_tai_khoan($_SESSION["tai_khoan"]);

		//Model don_hang & chi_tiet_don_hang
		include("app/models/m_don_hang.php");
		$m_don_hang = new M_don_hang();
		$don_hangs = $m_don_hang->Doc_don_hang_theo_kh($_SESSION["ma_khach_hang"]);
		$cthds = $m_don_hang->Doc_chi_tiet_hoa_don($_SESSION["ma_khach_hang"]);

		//View
		$title = "Trang tài khoản $tk->tai_khoan";
		$view = "app/views/khachhang/v_thongtin.php";
		include("public/include/layout_pages.php");
	}
}
?>