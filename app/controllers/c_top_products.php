<?php 
include("app/models/m_top_products.php");
class C_top_products
{
	public function Hien_thi_top()
	{
		//Model
		$m_top_products = new M_top_products();
		$apples = $m_top_products->Doc_top_apple();
		$samsungs = $m_top_products->Doc_top_samsung();
		$banchays = $m_top_products->Doc_top_banchay();
		//View
		include("app/views/top_products/v_top_products.php");
	}
}
?>