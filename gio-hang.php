<?php 
include_once("app/controllers/c_gio_hang.php");
$c_gio_hang = new C_gio_hang();

//Cập nhật giỏ hàng
if(isset($_POST["btnCapNhat"]))
{
	//Xóa sản phẩm trong giỏ
	$gio_hang = $c_gio_hang->cart();
	foreach ($gio_hang as $key => $value) {
		if(isset($_POST[$key]))
		{
			$c_gio_hang->Xoa_mat_hang($key, $_POST[$key]);
		}
	}
	//Cập nhật số lượng
	$gio_hang = $c_gio_hang->cart();
	if($gio_hang)
	{
        //$key: mã điện thoại, $value: số lượng cũ
		foreach ($gio_hang as $key => $value) {
			$sl_moi = $_POST["soluong".$key];
			if($sl_moi != $value)
			{
				$c_gio_hang->Cap_nhat_gio_hang($key, $sl_moi, $_POST["dongia".$key]);
			}
		}
	}
}

$c_gio_hang->Trang_gio_hang();
?>